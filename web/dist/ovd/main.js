(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./public/$$_lazy_route_resource lazy recursive":
/*!*************************************************************!*\
  !*** ./public/$$_lazy_route_resource lazy namespace object ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./public/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./public/app/app-routing.module.ts":
/*!******************************************!*\
  !*** ./public/app/app-routing.module.ts ***!
  \******************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./home/home.component */ "./public/app/home/home.component.ts");
/* harmony import */ var _auth_callback_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./auth/callback.component */ "./public/app/auth/callback.component.ts");
/* harmony import */ var _auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./auth/auth-guard.service */ "./public/app/auth/auth-guard.service.ts");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./auth/auth.service */ "./public/app/auth/auth.service.ts");
/* harmony import */ var _auth_unauthorized_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./auth/unauthorized.component */ "./public/app/auth/unauthorized.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var appRoutes = [
    { path: 'home', component: _home_home_component__WEBPACK_IMPORTED_MODULE_2__["HomeComponent"], canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]] },
    //{ path: 'contract', component:  ContractComponent, canActivate:[AuthGuard]},
    { path: 'login-callback', component: _auth_callback_component__WEBPACK_IMPORTED_MODULE_3__["LoginCallbackComponent"] },
    { path: '', redirectTo: 'home', pathMatch: 'full', canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]] },
    { path: 'unauthorized', component: _auth_unauthorized_component__WEBPACK_IMPORTED_MODULE_6__["UnAuhtorizedComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(appRoutes),
            ],
            exports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]
            ],
            providers: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"], _auth_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"]],
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./public/app/app.component.css":
/*!**************************************!*\
  !*** ./public/app/app.component.css ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nav {\n    padding: 0px;\n    position: absolute;\n    width: 100%;\n    margin-bottom: 0;\n    padding: 5px;\n}\n\nmainApp {\n    margin-top: 52px;\n}\n\n.full-height {\n    height: 100%;\n}"

/***/ }),

/***/ "./public/app/app.component.html":
/*!***************************************!*\
  !*** ./public/app/app.component.html ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-light bg-light\">\n    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarTogglerDemo03\" aria-controls=\"navbarTogglerDemo03\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n    <span class=\"navbar-toggler-icon\"></span>\n  </button>\n    <a class=\"navbar-brand\" routerLink=\"home\" href=\"#\">\n        <img src=\"/public/assets/HighResV2.png\" width=\"30\" height=\"30\" alt=\"\">\n    </a>\n\n    <div class=\"collapse navbar-collapse\" id=\"navbarTogglerDemo03\">\n        <ul class=\"navbar-nav mr-auto mt-2 mt-lg-0\">\n            <li class=\"nav-item\">\n                <a class=\"nav-link\" routerLink=\"contract\" routerLinkActive=\"active\">Contrats <span class=\"sr-only\">(current)</span></a>\n            </li>\n            <li class=\"nav-item\">\n                <a class=\"nav-link\" href=\"#\" (click)=\"contractService.updateContracts()\">\n                    <img src=\"/node_modules/open-iconic/svg/loop-circular.svg\" alt=\"icon name\">\n                </a>\n            </li>\n            <li class=\"nav-item\">\n                <form class=\"form-inline my-2 my-lg-0\">\n\n                    <input #searchTerm id=\"searchTerm\" type=\"text\" class=\"form-control mr-sm-2\" placeholder=\"Nom ou prénom\">\n                    <button type=\"submit\" class=\"btn btn-outline-success my-2 my-sm-0\" (click)=\"search(searchTerm.value)\">Rechercher contrat</button>\n                </form>\n            </li>\n\n            <li class=\"nav-item dropdown\">\n                <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                Admin\n                </a>\n                <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n                    <a class=\"dropdown-item\" href=\"#\">PLOUF</a>\n                    <a class=\"dropdown-item\" href=\"#\">Workpackage</a>\n                    <a class=\"dropdown-item\" href=\"#\">Action with Workpackages</a>\n                    <a class=\"dropdown-item\" href=\"#\">Check attributes</a>\n                </div>\n            </li>\n            <li class=\"nav-item\" *ngIf=\"!authService.isAuthenticated()\">\n                <a class=\"nav-link\" (click)=\"authService.login()\">Login <span class=\"sr-only\">(current)</span></a>\n            </li>\n            <li class=\"nav-item\" *ngIf=\"authService.isAuthenticated()\">\n                <a class=\"nav-link\" (click)=\"authService.logout()\">Logout <span class=\"sr-only\">(current)</span></a>\n            </li>\n        </ul>\n    </div>\n</nav>\n\n<router-outlet class=\"row mainApp container\">\n\n</router-outlet>"

/***/ }),

/***/ "./public/app/app.component.ts":
/*!*************************************!*\
  !*** ./public/app/app.component.ts ***!
  \*************************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _contract_service_contract_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./contract/service/contract.service */ "./public/app/contract/service/contract.service.ts");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./auth/auth.service */ "./public/app/auth/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AppComponent = /** @class */ (function () {
    function AppComponent(router, authService, contractService) {
        this.router = router;
        this.authService = authService;
        this.contractService = contractService;
        this.title = 'OVD - Manager';
        //authService.handleAuthentication();
    }
    ;
    AppComponent.prototype.search = function (searchTerm) {
        this.router.navigate(["contract/search/" + searchTerm]);
    };
    AppComponent.prototype.login = function () {
        this.authService.login();
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./public/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./public/app/app.component.css")],
            providers: [_contract_service_contract_service__WEBPACK_IMPORTED_MODULE_2__["ContractService"], _auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"], _contract_service_contract_service__WEBPACK_IMPORTED_MODULE_2__["ContractService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./public/app/app.module.ts":
/*!**********************************!*\
  !*** ./public/app/app.module.ts ***!
  \**********************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./public/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./public/app/app.component.ts");
/* harmony import */ var _contract_contract_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contract/contract.module */ "./public/app/contract/contract.module.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./home/home.component */ "./public/app/home/home.component.ts");
/* harmony import */ var _auth_callback_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./auth/callback.component */ "./public/app/auth/callback.component.ts");
/* harmony import */ var _auth_unauthorized_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./auth/unauthorized.component */ "./public/app/auth/unauthorized.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                //MenuComponent,
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_7__["HomeComponent"],
                _auth_callback_component__WEBPACK_IMPORTED_MODULE_8__["LoginCallbackComponent"],
                _auth_unauthorized_component__WEBPACK_IMPORTED_MODULE_9__["UnAuhtorizedComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_3__["HttpModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_3__["JsonpModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                _contract_contract_module__WEBPACK_IMPORTED_MODULE_6__["ContractModule"],
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]],
            entryComponents: []
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./public/app/auth/auth-guard.service.ts":
/*!***********************************************!*\
  !*** ./public/app/auth/auth-guard.service.ts ***!
  \***********************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./auth.service */ "./public/app/auth/auth.service.ts");
// auth-guard.service.ts
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuard = /** @class */ (function () {
    function AuthGuard(auth, router) {
        this.auth = auth;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function () {
        if (this.auth.isAuthenticated()) {
            return true;
        }
        else {
            this.router.navigate(['unauthorized']);
            return false;
        }
    };
    AuthGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./public/app/auth/auth.service.ts":
/*!*****************************************!*\
  !*** ./public/app/auth/auth.service.ts ***!
  \*****************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var auth0_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! auth0-js */ "./node_modules/auth0-js/src/index.js");
/* harmony import */ var auth0_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(auth0_js__WEBPACK_IMPORTED_MODULE_2__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthService = /** @class */ (function () {
    function AuthService(router) {
        this.router = router;
        this.auth0 = new auth0_js__WEBPACK_IMPORTED_MODULE_2__["WebAuth"]({
            clientID: 'lVP58vHgTttL3fuZtJfUJRfJDPUJklNE',
            domain: 'onvousdemenage.eu.auth0.com',
            responseType: 'token id_token',
            audience: 'https://onvousdemenage.eu.auth0.com/userinfo',
            redirectUri: (!/localhost/.test(document.location.host)) ? "http://ovd.locateme.ch/login-callback" : "http://localhost:8000/login-callback",
            scope: 'openid'
        });
    }
    AuthService.prototype.login = function () {
        this.auth0.authorize();
    };
    AuthService.prototype.logout = function () {
        // Remove tokens and expiry time from localStorage
        localStorage.removeItem('access_token');
        localStorage.removeItem('id_token');
        localStorage.removeItem('expires_at');
        // Go back to the home route
        this.router.navigate(['/']);
    };
    AuthService.prototype.isAuthenticated = function () {
        // Check whether the current time is past the
        // access token's expiry time
        /*const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
        return new Date().getTime() < expiresAt;*/
        return true;
    };
    AuthService.prototype.setSession = function (authResult) {
        // Set the time that the access token will expire at
        var expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
        localStorage.setItem('access_token', authResult.accessToken);
        localStorage.setItem('id_token', authResult.idToken);
        localStorage.setItem('expires_at', expiresAt);
    };
    AuthService.prototype.handleAuthentication = function () {
        var _this = this;
        this.auth0.parseHash(function (err, authResult) {
            if (authResult && authResult.accessToken && authResult.idToken) {
                window.location.hash = '';
                _this.setSession(authResult);
                _this.router.navigate(['/home']);
            }
            else if (err) {
                _this.router.navigate(['/home']);
                console.log(err);
            }
        });
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./public/app/auth/callback.component.css":
/*!************************************************!*\
  !*** ./public/app/auth/callback.component.css ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".center-middle {\n    align-items: center;\n    vertical-align: middle;\n    text-align: center;\n    width: 100%;\n    height: 100%;\n    margin-top: 100px;\n}"

/***/ }),

/***/ "./public/app/auth/callback.component.html":
/*!*************************************************!*\
  !*** ./public/app/auth/callback.component.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "TEST"

/***/ }),

/***/ "./public/app/auth/callback.component.ts":
/*!***********************************************!*\
  !*** ./public/app/auth/callback.component.ts ***!
  \***********************************************/
/*! exports provided: LoginCallbackComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginCallbackComponent", function() { return LoginCallbackComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var LoginCallbackComponent = /** @class */ (function () {
    function LoginCallbackComponent() {
    }
    LoginCallbackComponent.prototype.ngOnInit = function () {
    };
    LoginCallbackComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            template: __webpack_require__(/*! ./callback.component.html */ "./public/app/auth/callback.component.html"),
            styles: [__webpack_require__(/*! ./callback.component.css */ "./public/app/auth/callback.component.css")]
        })
    ], LoginCallbackComponent);
    return LoginCallbackComponent;
}());



/***/ }),

/***/ "./public/app/auth/unauthorized.component.css":
/*!****************************************************!*\
  !*** ./public/app/auth/unauthorized.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".center-middle {\n    align-items: center;\n    vertical-align: middle;\n    text-align: center;\n    width: 100%;\n    height: 100%;\n    margin-top: 100px;\n}"

/***/ }),

/***/ "./public/app/auth/unauthorized.component.html":
/*!*****************************************************!*\
  !*** ./public/app/auth/unauthorized.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"center-middle\">\n    <button class=\"btn btn-success btn-lg\" (click)=\"auth.login()\">SE LOGGUER</button>\n</div>"

/***/ }),

/***/ "./public/app/auth/unauthorized.component.ts":
/*!***************************************************!*\
  !*** ./public/app/auth/unauthorized.component.ts ***!
  \***************************************************/
/*! exports provided: UnAuhtorizedComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UnAuhtorizedComponent", function() { return UnAuhtorizedComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./auth.service */ "./public/app/auth/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UnAuhtorizedComponent = /** @class */ (function () {
    function UnAuhtorizedComponent(auth) {
        this.auth = auth;
    }
    UnAuhtorizedComponent.prototype.ngOnInit = function () {
    };
    UnAuhtorizedComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            template: __webpack_require__(/*! ./unauthorized.component.html */ "./public/app/auth/unauthorized.component.html"),
            styles: [__webpack_require__(/*! ./unauthorized.component.css */ "./public/app/auth/unauthorized.component.css")]
        }),
        __metadata("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]])
    ], UnAuhtorizedComponent);
    return UnAuhtorizedComponent;
}());



/***/ }),

/***/ "./public/app/contract/contract-basic-info/basic-info.ts":
/*!***************************************************************!*\
  !*** ./public/app/contract/contract-basic-info/basic-info.ts ***!
  \***************************************************************/
/*! exports provided: BasicInfo, Status */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BasicInfo", function() { return BasicInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Status", function() { return Status; });
var BasicInfo = /** @class */ (function () {
    function BasicInfo(name, firstname, phoneNumber, email, birthDate, contactMethod, street, city, floor, movingDate, sendMethod, newStreet, newCity, newFloor, status, submitDate) {
        this.name = name;
        this.firstname = firstname;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.birthDate = birthDate;
        this.contactMethod = contactMethod;
        this.street = street;
        this.city = city;
        this.floor = floor;
        this.movingDate = movingDate;
        this.sendMethod = sendMethod;
        this.newStreet = newStreet;
        this.newCity = newCity;
        this.newFloor = newFloor;
        this.status = status;
        this.submitDate = submitDate;
    }
    return BasicInfo;
}());

var Status = /** @class */ (function () {
    function Status(workpackageTodo, workpackageInProgress, workpackageDone) {
        this.workpackageTodo = workpackageTodo;
        this.workpackageInProgress = workpackageInProgress;
        this.workpackageDone = workpackageDone;
    }
    return Status;
}());



/***/ }),

/***/ "./public/app/contract/contract-basic-info/contract-basic-info.component.css":
/*!***********************************************************************************!*\
  !*** ./public/app/contract/contract-basic-info/contract-basic-info.component.css ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".basic-info-box {\n    width: 100%;\n}\n\nh2 {\n    font-size: 1.4em;\n}\n\n.align-right {\n    float: right;\n}"

/***/ }),

/***/ "./public/app/contract/contract-basic-info/contract-basic-info.component.html":
/*!************************************************************************************!*\
  !*** ./public/app/contract/contract-basic-info/contract-basic-info.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\n    <div class=\"basic-info-box card-body\">\n        <div class=\"row\">\n            <div class=\"col-md-8\">\n                <span>{{basicInfo.name}} {{basicInfo.firstname}}</span>\n            </div>\n            <div class=\"col-md-4\">\n                <button type=\"button\" class=\"btn btn-info btn-sm align-right\" disabled>Modifier</button>\n                <button type=\"button\" class=\"btn btn-danger btn-sm align-right\" disabled>Archiver</button>\n            </div>\n\n            <div class=\"col-md-4\">\n                <div>{{basicInfo.phoneNumber}} </div>\n                <div>{{basicInfo.birthDate | date:'dd/MM/yyyy'}} </div>\n                <div>{{basicInfo.contactMethod}}</div>\n\n            </div>\n            <div class=\"col-md-3\">\n                <div> {{basicInfo.street}}</div>\n                <div> {{basicInfo.city}} </div>\n                <div> {{basicInfo.floor}} </div>\n            </div>\n            <div class=\"col-md-2\">\n                <span class=\"glyphicon glyphicon-arrow-right\" aria-hidden=\"true\"></span>\n\n                <div>{{basicInfo.movingDate | date:'dd/MM/yyyy'}}</div>\n            </div>\n            <div class=\"col-md-3\">\n                <div> {{basicInfo.newStreet}} </div>\n                <div> {{basicInfo.newCity}} </div>\n                <div> {{basicInfo.newFloor}} </div>\n\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./public/app/contract/contract-basic-info/contract-basic-info.component.ts":
/*!**********************************************************************************!*\
  !*** ./public/app/contract/contract-basic-info/contract-basic-info.component.ts ***!
  \**********************************************************************************/
/*! exports provided: ContractBasicInfoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContractBasicInfoComponent", function() { return ContractBasicInfoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _contract_basic_info_basic_info__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../contract-basic-info/basic-info */ "./public/app/contract/contract-basic-info/basic-info.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ContractBasicInfoComponent = /** @class */ (function () {
    function ContractBasicInfoComponent() {
    }
    ContractBasicInfoComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("basic-info"),
        __metadata("design:type", _contract_basic_info_basic_info__WEBPACK_IMPORTED_MODULE_1__["BasicInfo"])
    ], ContractBasicInfoComponent.prototype, "basicInfo", void 0);
    ContractBasicInfoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'contract-basic-info',
            template: __webpack_require__(/*! ./contract-basic-info.component.html */ "./public/app/contract/contract-basic-info/contract-basic-info.component.html"),
            styles: [__webpack_require__(/*! ./contract-basic-info.component.css */ "./public/app/contract/contract-basic-info/contract-basic-info.component.css")]
        })
    ], ContractBasicInfoComponent);
    return ContractBasicInfoComponent;
}());



/***/ }),

/***/ "./public/app/contract/contract-detail/contract-detail.component.css":
/*!***************************************************************************!*\
  !*** ./public/app/contract/contract-detail/contract-detail.component.css ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "contract-workpackage-list {\n    display: block;\n    height: 100%;\n    position: relative;\n    top: -145px;\n    padding-top: 145px;\n    margin-left: 0px;\n    margin-right: 0px;\n}\n\n.contract {\n    height: 95%;\n}"

/***/ }),

/***/ "./public/app/contract/contract-detail/contract-detail.component.html":
/*!****************************************************************************!*\
  !*** ./public/app/contract/contract-detail/contract-detail.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"contract\" class=\"contract\">\n    <contract-basic-info [basic-info]=\"contract.basicInfo\" class=\"\"></contract-basic-info>\n\n    <contract-workpackage-list class=\"workpackage-list\" [contract]=\"contract\"></contract-workpackage-list>\n</div>"

/***/ }),

/***/ "./public/app/contract/contract-detail/contract-detail.component.ts":
/*!**************************************************************************!*\
  !*** ./public/app/contract/contract-detail/contract-detail.component.ts ***!
  \**************************************************************************/
/*! exports provided: ContractDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContractDetailComponent", function() { return ContractDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _contract__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../contract */ "./public/app/contract/contract.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ContractDetailComponent = /** @class */ (function () {
    function ContractDetailComponent() {
    }
    ContractDetailComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _contract__WEBPACK_IMPORTED_MODULE_1__["Contract"])
    ], ContractDetailComponent.prototype, "contract", void 0);
    ContractDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'contract-detail',
            template: __webpack_require__(/*! ./contract-detail.component.html */ "./public/app/contract/contract-detail/contract-detail.component.html"),
            styles: [__webpack_require__(/*! ./contract-detail.component.css */ "./public/app/contract/contract-detail/contract-detail.component.css")]
        })
    ], ContractDetailComponent);
    return ContractDetailComponent;
}());



/***/ }),

/***/ "./public/app/contract/contract-list/contract-list.component.css":
/*!***********************************************************************!*\
  !*** ./public/app/contract/contract-list/contract-list.component.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "contract-list {\n    position: absolute;\n    display: block;\n    width: 100%;\n    height: 100%;\n}\n\n.contract-detail {\n    display: block;\n}\n\n.contract-list {\n    height: 100%;\n    width: 100%;\n    padding-top: 0px;\n    margin-bottom: 0px;\n    position: absolute;\n    top: 52px;\n    padding-bottom: 25px;\n}\n\n.link {\n    cursor: pointer;\n}\n\n.contract-detail {\n    height: 100%;\n    width: auto;\n    padding-top: 0px;\n    margin-bottom: 0px;\n}\n\n.contract-list .first-line {\n    font-weight: bold;\n}\n\n.contract-list .second-line {\n    font-size: 0.8em;\n}\n\n.contract-list .third-line {\n    font-size: 0.8em;\n}\n\n.overflow {\n    max-height: 95%;\n    overflow-y: auto;\n}\n\nli {\n    list-style-type: none;\n}\n\nul {\n    padding: 0;\n}\n\n.list-contract-item {\n    display: block;\n    margin: 3px;\n    padding: 2px;\n    border: solid 2px blue;\n}"

/***/ }),

/***/ "./public/app/contract/contract-list/contract-list.component.html":
/*!************************************************************************!*\
  !*** ./public/app/contract/contract-list/contract-list.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"full-div\" *ngIf=\"loading\">\n    <div class=\"waiting-box\">\n        {{loadingMessage}}\n        <div class=\"progress\">\n            <div class=\"progress-bar progress-bar-striped full-div\" role=\"progressbar\" aria-valuenow=\"45\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 100%\">\n                <span class=\"sr-only\">45% Complete</span>\n            </div>\n        </div>\n    </div>\n</div>\n<div class=\"contract-list row\">\n    <div class=\"overflow col-md-4\">\n        <ul class=\"list-group\">\n            <li class=\"list-group-item container\" *ngFor=\"let contract of contracts | orderBy:'test'\" (click)=\"onSelect(contract)\" [ngClass]=\"{'active': selectedContract == contract}\">\n                <div class=\"row\">\n                    <div class=\"col-md-8 link\">\n                        <div class=\"first-line\">{{contract.basicInfo.name }} {{contract.basicInfo.firstname }}</div>\n                        <div class=\"second-line\">Date de déménagement : {{contract.basicInfo.movingDate | date:'dd.MM.yyyy' }}</div>\n                        <div class=\"third-line\">Remplie le : {{contract.basicInfo.submitDate | date:'dd.MM.yyyy HH:mm' }}</div>\n                    </div>\n                    <div class=\"col-md-4\">\n                        <div class=\"progress\">\n                            <div class=\"progress-bar bg-success\" role=\"progressbar\" [ngStyle]=\"getSize(contract.basicInfo.status, contract.basicInfo.status.workpackageDone)\">\n                                <span class=\"sr-only\">{{(contract.basicInfo.status.workpackageDone/(contract.basicInfo.status.workpackageDone+contract.basicInfo.status.workpackageTodo+contract.basicInfo.status.workpackageInProgress))*100}}% Complete (success)</span>\n                            </div>\n                            <div class=\"progress-bar bg-warning progress-bar-striped\" role=\"progressbar\" [ngStyle]=\"getSize(contract.basicInfo.status, contract.basicInfo.status.workpackageInProgress)\">\n                                <span class=\"sr-only\">20% Complete (warning)</span>\n                            </div>\n                            <div class=\"progress-bar bg-danger\" role=\"progressbar\" [ngStyle]=\"getSize(contract.basicInfo.status, contract.basicInfo.status.workpackageTodo)\">\n                                <span class=\"sr-only\">10% Complete (danger)</span>\n                            </div>\n                        </div>\n                        <span class=\"badge badge-primary\">{{(contract.basicInfo.status.workpackageDone+contract.basicInfo.status.workpackageInProgress+contract.basicInfo.status.workpackageTodo)}}</span>\n                        <span class=\"badge badge-success\">{{contract.basicInfo.status.workpackageDone}}</span>\n                        <span class=\"badge badge-warning\">{{contract.basicInfo.status.workpackageInProgress}}</span>\n                        <span class=\"badge badge-danger\"> {{contract.basicInfo.status.workpackageTodo}}</span>\n                    </div>\n                </div>\n            </li>\n        </ul>\n    </div>\n    <contract-detail class=\"contract-detail col-md-8\" [contract]=\"selectedContract\"></contract-detail>\n</div>"

/***/ }),

/***/ "./public/app/contract/contract-list/contract-list.component.ts":
/*!**********************************************************************!*\
  !*** ./public/app/contract/contract-list/contract-list.component.ts ***!
  \**********************************************************************/
/*! exports provided: ContractListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContractListComponent", function() { return ContractListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _pipe_findValue_pipe__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../pipe/findValue.pipe */ "./public/app/contract/pipe/findValue.pipe.ts");
/* harmony import */ var _pipe_orderby_pipe__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../pipe/orderby.pipe */ "./public/app/contract/pipe/orderby.pipe.ts");
/* harmony import */ var _service_contract_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../service/contract.service */ "./public/app/contract/service/contract.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ContractListComponent = /** @class */ (function () {
    function ContractListComponent(contractService, findValueByNamePipe, route, router) {
        this.contractService = contractService;
        this.findValueByNamePipe = findValueByNamePipe;
        this.route = route;
        this.router = router;
        this.loading = true;
        this.loadingMessage = "Chargement des contracts en cours...";
    }
    ;
    ContractListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var contractsPromise = this.contractService.getContracts();
        contractsPromise.then(function (contracts) { _this.loading = false; _this.contracts = contracts; });
        this.route.params.subscribe(function (params) { return params["id"] && _this.contractService.getContract(params["id"]).then(function (contract) { return _this.selectedContract = contract; }); });
    };
    ContractListComponent.prototype.onSelect = function (contract) {
        this.selectedContract = contract;
    };
    ContractListComponent.prototype.filterByName = function (contract, term) {
        var nameValue = this.findValueByNamePipe.transform(contract.questionsAnswers, 'Name').toLowerCase();
        var firstNameValue = this.findValueByNamePipe.transform(contract.questionsAnswers, 'Firstname').toLowerCase();
        return firstNameValue.indexOf(term.toLowerCase()) > -1 || nameValue.indexOf(term.toLowerCase()) > -1;
    };
    ContractListComponent.prototype.search = function (term) {
    };
    ContractListComponent.prototype.getSize = function (status, refNumber) {
        var total = status.workpackageInProgress + status.workpackageDone + status.workpackageTodo;
        var percent = Math.round(100 * (refNumber / total));
        return { 'width': percent + '%' };
    };
    ContractListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'contract-list',
            template: __webpack_require__(/*! ./contract-list.component.html */ "./public/app/contract/contract-list/contract-list.component.html"),
            styles: [__webpack_require__(/*! ./contract-list.component.css */ "./public/app/contract/contract-list/contract-list.component.css")],
            providers: [_service_contract_service__WEBPACK_IMPORTED_MODULE_4__["ContractService"], _pipe_findValue_pipe__WEBPACK_IMPORTED_MODULE_2__["FindValueByName"], _pipe_orderby_pipe__WEBPACK_IMPORTED_MODULE_3__["OrderByPipe"]]
        }),
        __metadata("design:paramtypes", [_service_contract_service__WEBPACK_IMPORTED_MODULE_4__["ContractService"],
            _pipe_findValue_pipe__WEBPACK_IMPORTED_MODULE_2__["FindValueByName"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], ContractListComponent);
    return ContractListComponent;
}());



/***/ }),

/***/ "./public/app/contract/contract-routing.module.ts":
/*!********************************************************!*\
  !*** ./public/app/contract/contract-routing.module.ts ***!
  \********************************************************/
/*! exports provided: ContractRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContractRoutingModule", function() { return ContractRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _contract_list_contract_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./contract-list/contract-list.component */ "./public/app/contract/contract-list/contract-list.component.ts");
/* harmony import */ var _auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../auth/auth-guard.service */ "./public/app/auth/auth-guard.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var mappingRoutes = [
    { path: 'contract', component: _contract_list_contract_list_component__WEBPACK_IMPORTED_MODULE_2__["ContractListComponent"], canActivate: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]] },
    { path: 'contract/:id', component: _contract_list_contract_list_component__WEBPACK_IMPORTED_MODULE_2__["ContractListComponent"] },
    { path: 'contract/search/:searchTerm', component: _contract_list_contract_list_component__WEBPACK_IMPORTED_MODULE_2__["ContractListComponent"] },
];
var ContractRoutingModule = /** @class */ (function () {
    function ContractRoutingModule() {
    }
    ContractRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(mappingRoutes),
            ],
            exports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]
            ],
            providers: [_auth_auth_guard_service__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]]
        })
    ], ContractRoutingModule);
    return ContractRoutingModule;
}());



/***/ }),

/***/ "./public/app/contract/contract-workpackages/add-status-form.component.ts":
/*!********************************************************************************!*\
  !*** ./public/app/contract/contract-workpackages/add-status-form.component.ts ***!
  \********************************************************************************/
/*! exports provided: AddStatusFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddStatusFormComponent", function() { return AddStatusFormComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _contract__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../contract */ "./public/app/contract/contract.ts");
/* harmony import */ var _pipe_findValue_pipe__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../pipe/findValue.pipe */ "./public/app/contract/pipe/findValue.pipe.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddStatusFormComponent = /** @class */ (function () {
    function AddStatusFormComponent(findValueByNamePipe) {
        this.findValueByNamePipe = findValueByNamePipe;
    }
    ;
    AddStatusFormComponent.prototype.ngOnInit = function () {
        this.modelStatus = new _contract__WEBPACK_IMPORTED_MODULE_1__["Status"]("", new Date(), "", "", 0, this.workpackage.id);
    };
    AddStatusFormComponent.prototype.addStatus = function () {
    };
    Object.defineProperty(AddStatusFormComponent.prototype, "diagnostic", {
        get: function () { return JSON.stringify(this.modelStatus); },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _contract__WEBPACK_IMPORTED_MODULE_1__["Workpackage"])
    ], AddStatusFormComponent.prototype, "workpackage", void 0);
    AddStatusFormComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'add-status-form',
            template: "\n            <div class=\"card\">\n            {{diagnostic}}\n              <form #statusForm=\"ngForm\">\n                <div class=\"form-row\">\n                  <div class=\"col-md-8\">\n                    <textarea class=\"form-control form-control-sm\" id=\"comments\" rows=\"1\" [(ngModel)]=\"modelStatus.comments\" name=\"comments\"></textarea>\n                  </div>\n                  <div class=\"col-md-3\">\n                    <select class=\"form-control form-control-sm\" name=\"status\" [(ngModel)]=\"modelStatus.status\" id=\"status\">\n                      <option>TODO</option>\n                      <option>TEST</option>\n                    </select>\n                  </div>\n                  <div class=\"col-md-1\">\n                    <button type=\"submit\" class=\"btn btn-primary btn-sm\">Sauver</button>\n                  </div>\n                </div>\n              </form>\n            </div>\n            ",
            providers: [_pipe_findValue_pipe__WEBPACK_IMPORTED_MODULE_2__["FindValueByName"]]
        }),
        __metadata("design:paramtypes", [_pipe_findValue_pipe__WEBPACK_IMPORTED_MODULE_2__["FindValueByName"]])
    ], AddStatusFormComponent);
    return AddStatusFormComponent;
}());



/***/ }),

/***/ "./public/app/contract/contract-workpackages/contract-workpackage-list.component.css":
/*!*******************************************************************************************!*\
  !*** ./public/app/contract/contract-workpackages/contract-workpackage-list.component.css ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".list-workpackage {\n    padding: 0;\n    overflow-y: auto;\n}\n\n.workpackages {\n    height: 100%;\n    margin-right: 0px;\n    margin-left: 0px;\n}\n\n.overflow {\n    overflow-y: auto;\n}\n\n.workpackage-detail {\n    margin: 0px;\n    padding: 0px;\n}\n\n.statusBox {\n    max-height: 50%;\n}"

/***/ }),

/***/ "./public/app/contract/contract-workpackages/contract-workpackage-list.component.html":
/*!********************************************************************************************!*\
  !*** ./public/app/contract/contract-workpackages/contract-workpackage-list.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row workpackages\">\n    <div class=\"col-md-3 list-workpackage\">\n        <ul class=\"list-group\">\n            <li class=\"list-group-item container \" *ngFor=\"let workpackage of contract.workpackages\" id=\"wp-{{workpackage.id}}\" (click)=\"selectWorkPackage(workpackage)\" [ngClass]=\"{'active': selectedWorkpackage == workpackage}\">\n                <div class=\"input-group success mb-2 mb-sm-0\">\n                    <div [contractStatus]=\"workpackage\" class=\"input-group-addon\"><img src=\"/node_modules/open-iconic/svg/check.svg\" alt=\"icon name\">\n                    </div>\n                    <button type=\"button\" class=\"form-control\">{{workpackage.name}} </button>\n                </div>\n\n            </li>\n        </ul>\n    </div>\n    <div class=\"col-md-9 row overflow workpackage-detail\" *ngIf=\"selectedWorkpackage\">\n        <div class=\"col-md-12 statusBox\">\n            <show-status [workpackage]=\"selectedWorkpackage\"></show-status>\n            <add-status-form [workpackage]=\"selectedWorkpackage\"></add-status-form>\n        </div>\n        <div class=\"col-md-8\">\n            <show-answers [answers]=\"contract.questionsAnswers\" [fieldsToShow]=\"selectedWorkpackage.needed_fields\"></show-answers>\n        </div>\n        <div class=\"col-md-4\">\n            <show-actions [workpackage]=\"selectedWorkpackage\"></show-actions>\n        </div>\n    </div>\n\n\n</div>\n<contract-detail [contract]=\"selectedContract\"></contract-detail>"

/***/ }),

/***/ "./public/app/contract/contract-workpackages/contract-workpackage-list.component.ts":
/*!******************************************************************************************!*\
  !*** ./public/app/contract/contract-workpackages/contract-workpackage-list.component.ts ***!
  \******************************************************************************************/
/*! exports provided: ContractWorkpackageList */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContractWorkpackageList", function() { return ContractWorkpackageList; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _contract__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../contract */ "./public/app/contract/contract.ts");
/* harmony import */ var _pipe_findValue_pipe__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../pipe/findValue.pipe */ "./public/app/contract/pipe/findValue.pipe.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ContractWorkpackageList = /** @class */ (function () {
    function ContractWorkpackageList(findValueByNamePipe) {
        this.findValueByNamePipe = findValueByNamePipe;
    }
    ContractWorkpackageList.prototype.ngOnInit = function () {
    };
    ;
    ContractWorkpackageList.prototype.selectWorkPackage = function (workpackage) {
        this.selectedWorkpackage = workpackage;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _contract__WEBPACK_IMPORTED_MODULE_1__["Contract"])
    ], ContractWorkpackageList.prototype, "contract", void 0);
    ContractWorkpackageList = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'contract-workpackage-list',
            template: __webpack_require__(/*! ./contract-workpackage-list.component.html */ "./public/app/contract/contract-workpackages/contract-workpackage-list.component.html"),
            styles: [__webpack_require__(/*! ./contract-workpackage-list.component.css */ "./public/app/contract/contract-workpackages/contract-workpackage-list.component.css")],
            providers: [_pipe_findValue_pipe__WEBPACK_IMPORTED_MODULE_2__["FindValueByName"]],
        }),
        __metadata("design:paramtypes", [_pipe_findValue_pipe__WEBPACK_IMPORTED_MODULE_2__["FindValueByName"]])
    ], ContractWorkpackageList);
    return ContractWorkpackageList;
}());



/***/ }),

/***/ "./public/app/contract/contract-workpackages/show-actions.component.ts":
/*!*****************************************************************************!*\
  !*** ./public/app/contract/contract-workpackages/show-actions.component.ts ***!
  \*****************************************************************************/
/*! exports provided: ShowActionsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShowActionsComponent", function() { return ShowActionsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _contract__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../contract */ "./public/app/contract/contract.ts");
/* harmony import */ var _service_contract_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/contract.service */ "./public/app/contract/service/contract.service.ts");
/* harmony import */ var _pipe_findValue_pipe__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../pipe/findValue.pipe */ "./public/app/contract/pipe/findValue.pipe.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ShowActionsComponent = /** @class */ (function () {
    function ShowActionsComponent(findValueByNamePipe, contractService) {
        this.findValueByNamePipe = findValueByNamePipe;
        this.contractService = contractService;
    }
    ;
    ShowActionsComponent.prototype.fireAction = function (action) {
        switch (action.type) {
            case "EXTERNAL_LINK":
                this.openExternalLink(action);
                break;
            default:
        }
    };
    ShowActionsComponent.prototype.openExternalLink = function (action) {
        //var url = this.contractService.replaceVarByValue(action.action, );
        var newWindow = window.open('some_url');
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _contract__WEBPACK_IMPORTED_MODULE_1__["Workpackage"])
    ], ShowActionsComponent.prototype, "workpackage", void 0);
    ShowActionsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'show-actions',
            template: "<div class=\"card\">\n              <div class=\"card-header\">Actions possible</div>\n              <ul class=\"items card-body\">\n              <li class=\"row\" *ngFor=\"let action of workpackage.actions\">\n                  \n                  <div class=\"col-md-12\">\n                    <button class=\"btn btn-primary\" type=\"submit\" (click)=\"fireAction(action)\">{{action.action_name}}</button>\n                  </div>\n\n                  <div class=\"col-md-12\"><b>Que faire : </b> {{action.comments}}</div>\n              </li>\n              </ul>\n            </div>\n              ",
            providers: [_pipe_findValue_pipe__WEBPACK_IMPORTED_MODULE_3__["FindValueByName"]]
        }),
        __metadata("design:paramtypes", [_pipe_findValue_pipe__WEBPACK_IMPORTED_MODULE_3__["FindValueByName"],
            _service_contract_service__WEBPACK_IMPORTED_MODULE_2__["ContractService"]])
    ], ShowActionsComponent);
    return ShowActionsComponent;
}());



/***/ }),

/***/ "./public/app/contract/contract-workpackages/show-answers.component.ts":
/*!*****************************************************************************!*\
  !*** ./public/app/contract/contract-workpackages/show-answers.component.ts ***!
  \*****************************************************************************/
/*! exports provided: ShowAnswersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShowAnswersComponent", function() { return ShowAnswersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _pipe_findValue_pipe__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../pipe/findValue.pipe */ "./public/app/contract/pipe/findValue.pipe.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ShowAnswersComponent = /** @class */ (function () {
    function ShowAnswersComponent(findValueByNamePipe) {
        this.findValueByNamePipe = findValueByNamePipe;
    }
    ShowAnswersComponent.prototype.getFields = function () {
        var fields = this.fieldsToShow.split(',');
        var fieldsRet = [];
        for (var index = 0; index < fields.length; index++) {
            var field = fields[index];
            var q = this.getQuestion(field);
            var a = this.getAnswer(field);
            if (q && a != null && q != null) {
                fieldsRet.push({ 'q': q, 'a': a });
            }
        }
        return fieldsRet;
    };
    ;
    ShowAnswersComponent.prototype.getQuestion = function (field) {
        if (field === "") {
            return null;
        }
        var answer = this.answers.filter(function (answer) { return answer.mapping && answer.mapping.toLowerCase() === field.toLowerCase(); });
        return answer && answer.length > 0 && answer[0].question;
    };
    ShowAnswersComponent.prototype.getAnswer = function (field) {
        return this.findValueByNamePipe.transform(this.answers, field);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Array)
    ], ShowAnswersComponent.prototype, "answers", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], ShowAnswersComponent.prototype, "fieldsToShow", void 0);
    ShowAnswersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'show-answers',
            template: "<div class=\"card\">\n              <div class=\"card-header\">Information sur le client</div>\n              <ul class=\"items card-body\">\n              <li class=\"row\" *ngFor=\"let field of getFields()\">\n                  <div class=\"col-md-6\">{{field.q}}</div>\n                  <div class=\"col-md-6\">{{field.a}}</div>\n              </li>\n              </ul>\n            </div>\n              ",
            providers: [_pipe_findValue_pipe__WEBPACK_IMPORTED_MODULE_1__["FindValueByName"]]
        }),
        __metadata("design:paramtypes", [_pipe_findValue_pipe__WEBPACK_IMPORTED_MODULE_1__["FindValueByName"]])
    ], ShowAnswersComponent);
    return ShowAnswersComponent;
}());



/***/ }),

/***/ "./public/app/contract/contract-workpackages/show-status.component.ts":
/*!****************************************************************************!*\
  !*** ./public/app/contract/contract-workpackages/show-status.component.ts ***!
  \****************************************************************************/
/*! exports provided: ShowStatusComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShowStatusComponent", function() { return ShowStatusComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _contract__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../contract */ "./public/app/contract/contract.ts");
/* harmony import */ var _pipe_findValue_pipe__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../pipe/findValue.pipe */ "./public/app/contract/pipe/findValue.pipe.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ShowStatusComponent = /** @class */ (function () {
    function ShowStatusComponent(findValueByNamePipe) {
        this.findValueByNamePipe = findValueByNamePipe;
    }
    ;
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _contract__WEBPACK_IMPORTED_MODULE_1__["Workpackage"])
    ], ShowStatusComponent.prototype, "workpackage", void 0);
    ShowStatusComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'show-status',
            template: "\n            <div class=\"card\">\n              <div class=\"card-header\">Historique</div>\n              <div *ngIf=\"workpackage.statuses.length < 1\">Pas de commentaire</div>\n              <ul class=\"items card-body\">\n              <li class=\"row\" *ngFor=\"let status of workpackage.statuses\">\n                  <div class=\"col-md-3\">{{status.creation_date}}</div>\n                  <div class=\"col-md-6\">{{status.comments}}</div>\n                  <div class=\"col-md-3\">{{status.status}}</div>\n              </li>\n              </ul>\n            </div>\n            ",
            providers: [_pipe_findValue_pipe__WEBPACK_IMPORTED_MODULE_2__["FindValueByName"]]
        }),
        __metadata("design:paramtypes", [_pipe_findValue_pipe__WEBPACK_IMPORTED_MODULE_2__["FindValueByName"]])
    ], ShowStatusComponent);
    return ShowStatusComponent;
}());



/***/ }),

/***/ "./public/app/contract/contract.module.ts":
/*!************************************************!*\
  !*** ./public/app/contract/contract.module.ts ***!
  \************************************************/
/*! exports provided: ContractModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContractModule", function() { return ContractModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _contract_list_contract_list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./contract-list/contract-list.component */ "./public/app/contract/contract-list/contract-list.component.ts");
/* harmony import */ var _contract_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./contract-routing.module */ "./public/app/contract/contract-routing.module.ts");
/* harmony import */ var _service_contract_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./service/contract.service */ "./public/app/contract/service/contract.service.ts");
/* harmony import */ var _contract_detail_contract_detail_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contract-detail/contract-detail.component */ "./public/app/contract/contract-detail/contract-detail.component.ts");
/* harmony import */ var _contract_basic_info_contract_basic_info_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./contract-basic-info/contract-basic-info.component */ "./public/app/contract/contract-basic-info/contract-basic-info.component.ts");
/* harmony import */ var _pipe_findValue_pipe__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./pipe/findValue.pipe */ "./public/app/contract/pipe/findValue.pipe.ts");
/* harmony import */ var _contract_workpackages_contract_workpackage_list_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./contract-workpackages/contract-workpackage-list.component */ "./public/app/contract/contract-workpackages/contract-workpackage-list.component.ts");
/* harmony import */ var _contract_workpackages_show_answers_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./contract-workpackages/show-answers.component */ "./public/app/contract/contract-workpackages/show-answers.component.ts");
/* harmony import */ var _contract_workpackages_show_status_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./contract-workpackages/show-status.component */ "./public/app/contract/contract-workpackages/show-status.component.ts");
/* harmony import */ var _contract_workpackages_show_actions_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./contract-workpackages/show-actions.component */ "./public/app/contract/contract-workpackages/show-actions.component.ts");
/* harmony import */ var _contract_workpackages_add_status_form_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./contract-workpackages/add-status-form.component */ "./public/app/contract/contract-workpackages/add-status-form.component.ts");
/* harmony import */ var _pipe_orderby_pipe__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./pipe/orderby.pipe */ "./public/app/contract/pipe/orderby.pipe.ts");
/* harmony import */ var _directives_contract_status_directive__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./directives/contract-status.directive */ "./public/app/contract/directives/contract-status.directive.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















var ContractModule = /** @class */ (function () {
    function ContractModule() {
    }
    ContractModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _contract_routing_module__WEBPACK_IMPORTED_MODULE_4__["ContractRoutingModule"]
            ],
            declarations: [_contract_list_contract_list_component__WEBPACK_IMPORTED_MODULE_3__["ContractListComponent"], _contract_detail_contract_detail_component__WEBPACK_IMPORTED_MODULE_6__["ContractDetailComponent"],
                _contract_basic_info_contract_basic_info_component__WEBPACK_IMPORTED_MODULE_7__["ContractBasicInfoComponent"], _contract_workpackages_contract_workpackage_list_component__WEBPACK_IMPORTED_MODULE_9__["ContractWorkpackageList"], _contract_workpackages_show_answers_component__WEBPACK_IMPORTED_MODULE_10__["ShowAnswersComponent"],
                _contract_workpackages_show_actions_component__WEBPACK_IMPORTED_MODULE_12__["ShowActionsComponent"], _contract_workpackages_show_status_component__WEBPACK_IMPORTED_MODULE_11__["ShowStatusComponent"], _contract_workpackages_add_status_form_component__WEBPACK_IMPORTED_MODULE_13__["AddStatusFormComponent"], _pipe_findValue_pipe__WEBPACK_IMPORTED_MODULE_8__["FindValueByName"], _pipe_orderby_pipe__WEBPACK_IMPORTED_MODULE_14__["OrderByPipe"], _directives_contract_status_directive__WEBPACK_IMPORTED_MODULE_15__["ContractStatusDirective"]],
            providers: [_service_contract_service__WEBPACK_IMPORTED_MODULE_5__["ContractService"]]
        })
    ], ContractModule);
    return ContractModule;
}());



/***/ }),

/***/ "./public/app/contract/contract.ts":
/*!*****************************************!*\
  !*** ./public/app/contract/contract.ts ***!
  \*****************************************/
/*! exports provided: Contract, Question, Workpackage, Status, Action */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Contract", function() { return Contract; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Question", function() { return Question; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Workpackage", function() { return Workpackage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Status", function() { return Status; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Action", function() { return Action; });
var Contract = /** @class */ (function () {
    function Contract(id, questionsAnswers, workpackages, basicInfo) {
        this.id = id;
        this.questionsAnswers = questionsAnswers;
        this.workpackages = workpackages;
        this.basicInfo = basicInfo;
    }
    return Contract;
}());

var Question = /** @class */ (function () {
    function Question(question, answer, mapping) {
        this.question = question;
        this.answer = answer;
        this.mapping = mapping;
    }
    return Question;
}());

var Workpackage = /** @class */ (function () {
    function Workpackage(id, name, neededFields, sortingPriority, to_print, statuses, actions, lastStatus) {
        this.id = id;
        this.name = name;
        this.neededFields = neededFields;
        this.sortingPriority = sortingPriority;
        this.to_print = to_print;
        this.statuses = statuses;
        this.actions = actions;
        this.lastStatus = lastStatus;
    }
    return Workpackage;
}());

var Status = /** @class */ (function () {
    function Status(comments, creation_date, form_id, status, status_id, work_package_id) {
        this.comments = comments;
        this.creation_date = creation_date;
        this.form_id = form_id;
        this.status = status;
        this.status_id = status_id;
        this.work_package_id = work_package_id;
    }
    return Status;
}());

var Action = /** @class */ (function () {
    function Action(action, action_name, comments, condition_attribut, condition_value, id, data, type, work_package_fk) {
        this.action = action;
        this.action_name = action_name;
        this.comments = comments;
        this.condition_attribut = condition_attribut;
        this.condition_value = condition_value;
        this.id = id;
        this.data = data;
        this.type = type;
        this.work_package_fk = work_package_fk;
    }
    return Action;
}());



/***/ }),

/***/ "./public/app/contract/directives/contract-status.directive.ts":
/*!*********************************************************************!*\
  !*** ./public/app/contract/directives/contract-status.directive.ts ***!
  \*********************************************************************/
/*! exports provided: ContractStatusDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContractStatusDirective", function() { return ContractStatusDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _contract__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../contract */ "./public/app/contract/contract.ts");
/* harmony import */ var _service_contract_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/contract.service */ "./public/app/contract/service/contract.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ContractStatusDirective = /** @class */ (function () {
    function ContractStatusDirective(el, renderer, contractService) {
        this.el = el;
        this.renderer = renderer;
        this.contractService = contractService;
    }
    ContractStatusDirective.prototype.ngOnInit = function () {
        switch (this.contractService.getLastStatus(this.contractStatus)) {
            case 'TODO':
                this.el.nativeElement.classList.add('bg-warning');
                break;
            case 'TO SEND':
                this.el.nativeElement.classList.add('bg-warning');
                break;
            case 'IN PROGRESS':
                this.el.nativeElement.classList.add('bg-warning');
                break;
            case 'WAITING_TRUE':
                this.el.nativeElement.classList.add('bg-warning');
                break;
            case 'DONE':
                this.el.nativeElement.classList.add('bg-success');
                break;
            case 'NOT APPLICABLE':
                this.el.nativeElement.classList.add('bg-success');
                break;
            default:
                this.el.nativeElement.classList.add('bg-danger');
                break;
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _contract__WEBPACK_IMPORTED_MODULE_1__["Workpackage"])
    ], ContractStatusDirective.prototype, "contractStatus", void 0);
    ContractStatusDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[contractStatus]',
            providers: [_service_contract_service__WEBPACK_IMPORTED_MODULE_2__["ContractService"]]
        }),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer"], _service_contract_service__WEBPACK_IMPORTED_MODULE_2__["ContractService"]])
    ], ContractStatusDirective);
    return ContractStatusDirective;
}());



/***/ }),

/***/ "./public/app/contract/pipe/findValue.pipe.ts":
/*!****************************************************!*\
  !*** ./public/app/contract/pipe/findValue.pipe.ts ***!
  \****************************************************/
/*! exports provided: FindValueByName */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FindValueByName", function() { return FindValueByName; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var FindValueByName = /** @class */ (function () {
    function FindValueByName() {
    }
    FindValueByName.prototype.transform = function (allValues, name) {
        var questionsFiltered = allValues.filter(function (question) { return question.mapping && name && question.mapping.toLowerCase() == name.toLowerCase(); });
        if (questionsFiltered.length == 0) {
            return "[VALUE NOT FOUND : " + name + "]";
        }
        return questionsFiltered[0].answer;
    };
    FindValueByName = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({ name: 'findValueByName' })
    ], FindValueByName);
    return FindValueByName;
}());



/***/ }),

/***/ "./public/app/contract/pipe/orderby.pipe.ts":
/*!**************************************************!*\
  !*** ./public/app/contract/pipe/orderby.pipe.ts ***!
  \**************************************************/
/*! exports provided: OrderByPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderByPipe", function() { return OrderByPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var OrderByPipe = /** @class */ (function () {
    function OrderByPipe() {
    }
    OrderByPipe.prototype.transform = function (array, args) {
        if (!array || array === undefined || array.length === 0)
            return null;
        array.sort(function (a, b) {
            if (a.basicInfo.movingDate < b.basicInfo.movingDate) {
                return 1;
            }
            else if (a.basicInfo.movingDate > b.basicInfo.movingDate) {
                return -1;
            }
            else {
                return 0;
            }
        });
        return array;
    };
    OrderByPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: 'orderBy'
        })
    ], OrderByPipe);
    return OrderByPipe;
}());



/***/ }),

/***/ "./public/app/contract/service/contract.service.ts":
/*!*********************************************************!*\
  !*** ./public/app/contract/service/contract.service.ts ***!
  \*********************************************************/
/*! exports provided: ContractService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContractService", function() { return ContractService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ContractService = /** @class */ (function () {
    function ContractService(http) {
        this.http = http;
        this.headers = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["Headers"]({
            'X-API-KEY': '612e648bf9594adb50844cad6895f2cf'
        });
        this.mappingsUrl = 'http://localhost:8000/api/v2/contracts'; // URL to web API
    }
    ContractService.prototype.updateContracts = function () {
        localStorage.removeItem("contracts");
        this.getContracts();
    };
    ContractService.prototype.getContracts = function () {
        var _this = this;
        var storageContracts = JSON.parse(localStorage.getItem("contracts"));
        if (typeof (storageContracts) != 'undefined' && storageContracts != null && storageContracts.length > 0) {
            return Promise.resolve(storageContracts);
        }
        return this.http.get(this.mappingsUrl, { headers: this.headers })
            .toPromise()
            .then(function (response) {
            var contracts = response.json();
            contracts = _this.populateBasicInfo(contracts);
            localStorage.setItem("contracts", JSON.stringify(contracts));
            return contracts;
            //return response.json() as Contract[];
        });
    };
    ContractService.prototype.getContract = function (id) {
        var storageContracts = JSON.parse(localStorage.getItem("contracts"));
        if (typeof (storageContracts) != 'undefined' && storageContracts != null && storageContracts.length > 0) {
            return storageContracts.find(function (contract) { return contract.id === id; });
        }
        return this.getContracts().then(function (contracts) { return contracts.find(function (contract) { return contract.id === id; }); });
    };
    ContractService.prototype.getValueForName = function (allValues, name) {
        var questionsFiltered = allValues.filter(function (question) { return question.mapping && name && question.mapping.toLowerCase() == name.toLowerCase(); });
        if (questionsFiltered.length == 0) {
            return "[VALUE NOT FOUND : " + name + "]";
        }
        return questionsFiltered[0].answer;
    };
    ContractService.prototype.handleError = function (error) {
        // In a real world app, you might use a remote logging infrastructure
        var errMsg;
        if (error instanceof _angular_http__WEBPACK_IMPORTED_MODULE_1__["Response"]) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Promise.call(errMsg);
    };
    ContractService.prototype.populateBasicInfo = function (contracts) {
        for (var index = 0; index < contracts.length; index++) {
            if (contract.questionsAnswers != null) {
                var contract = contracts[index];
                /* contract.basicInfo = new BasicInfo(
                             this.getValueForName(contract.questionsAnswers, "name"),
                             this.getValueForName(contract.questionsAnswers, "firstname"),
                             this.getValueForName(contract.questionsAnswers, "phoneNumber"),
                             this.getValueForName(contract.questionsAnswers, "email"),
                             new Date(this.getValueForName(contract.questionsAnswers, "birthdate")),
                             this.getValueForName(contract.questionsAnswers, "howToContactCustomer"),
                             this.getValueForName(contract.questionsAnswers, "street"),
                             this.getValueForName(contract.questionsAnswers, "city"),
                             this.getValueForName(contract.questionsAnswers, "Floor"),
                             new Date(this.getValueForName(contract.questionsAnswers, "movingDate")),
                             this.getValueForName(contract.questionsAnswers, "contactType"),
                             this.getValueForName(contract.questionsAnswers, "newStreet"),
                             this.getValueForName(contract.questionsAnswers, "newCity"),
                             this.getValueForName(contract.questionsAnswers, "NewFloor"),
                             this.getStatus(contract.questionsAnswers, contract.workpackages),
                             new Date(this.getValueForName(contract.questionsAnswers, "submitDate")),
                 );*/
            }
        }
        return contracts;
    };
    ContractService.prototype.getStatus = function (questions, workpackages) {
        var status = { workpackageDone: 0, workpackageInProgress: 0, workpackageTodo: 0 };
        for (var index = 0; index < workpackages.length; index++) {
            var workpackage = workpackages[index];
            workpackage.lastStatus = this.getLastStatus(workpackage);
            if (workpackage.lastStatus == null) {
                status.workpackageTodo++;
            }
            else {
                switch (workpackage.lastStatus) {
                    case "DONE":
                        status.workpackageDone++;
                        break;
                    case "IN PROGRESS":
                        status.workpackageInProgress++;
                        break;
                    case "WAITING_TRUE":
                        status.workpackageInProgress++;
                        break;
                    case "TO SEND":
                        status.workpackageInProgress++;
                        break;
                    case "TO_PRINT":
                        status.workpackageInProgress++;
                        break;
                    case "NOT APPLICABLE": break;
                    default:
                        status.workpackageTodo++;
                        break;
                }
            }
        }
        return status;
    };
    ContractService.prototype.getLastStatus = function (workpackage) {
        if (workpackage.statuses.length > 0) {
            workpackage.statuses.sort(function (a, b) {
                if (a.creation_date < b.creation_date) {
                    return 1;
                }
                else if (a.creation_date > b.creation_date) {
                    return -1;
                }
                else {
                    return 0;
                }
            });
            return workpackage.statuses[0].status;
        }
        return null;
    };
    ContractService.prototype.replaceVarByValue = function (strValue, answers) {
        var regExp = /{{[^}]*}}/;
        var dataVar = regExp.exec(strValue);
        var filledData = strValue;
        while (dataVar != null && dataVar.length > 0) {
            var varToReplace = dataVar[0].replace("{{", "").replace("}}", "");
            var valueFounded = this.getValue(answers, varToReplace);
            if (valueFounded.length > 0) {
                filledData = filledData.replace(dataVar[0], valueFounded[0].answer);
            }
            else {
                console.error("No answer founded for " + varToReplace);
                filledData = filledData.replace(dataVar[0], "ERROR: " + varToReplace);
                break;
            }
            dataVar = regExp.exec(filledData);
        }
        return filledData;
    };
    ContractService.prototype.getValue = function (scope, name) {
        return scope.filter(function (value) { return value.answer === name; });
    };
    ;
    ContractService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], ContractService);
    return ContractService;
}());



/***/ }),

/***/ "./public/app/home/home.component.css":
/*!********************************************!*\
  !*** ./public/app/home/home.component.css ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./public/app/home/home.component.html":
/*!*********************************************!*\
  !*** ./public/app/home/home.component.html ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n    <h2>Administration</h2>\n    <div>\n        <div>\n            <a href=\"https://smartbusiness.postfinance.ch/?rda=login\" target=\"_blank\">SmartBusiness</a> -- Gestion des factures BVR\n        </div>\n    </div>\n    <div>\n        <div>\n            <a href=\"https://www.postfinance.ch/ap/ba/fp/html/e-finance/home?login&WT.ac=_le_home_LoginHeaderE-Finance\" target=\"_blank\">PostFinance</a> -- Gestion du compte postal\n        </div>\n    </div>\n\n    <div>\n        <a href=\"https://mail.zoho.com/zm/#mail/folder/inbox\" target=\"_blank\">Mail Zoho @onvousdemenage.ch</a>\n    </div>\n    <div>\n        <a href=\"https://www.kickstarter.com/\" target=\"_blank\">KickStarter</a> - bonjour@onvoudemenage.ch\n    </div>\n    <div>\n        <a href=\"https://secure.vistaprint.ch/vp/ns/sign_in.aspx?xnav=top&noguest=1\" target=\"_blank\">Vistaprint</a> - bonjour@onvoudemenage.ch\n    </div>\n    <div>\n        <a href=\"https://business.google.com/b/106786088978881286949/dashboard/l/17399768989209058819?hl=de&ppsrc=GMBS0&gmbsrc=ch-fr-ha-se-z-gmb-s-z-h~bk-46170212063-c&utm_campaign=ch-fr-ha-se-z-gmb-s-z-h~bk-46170212063-c&utm_medium=ha&utm_source=gmb\" target=\"_blank\">Google My Business</a>        Pour configurer ce qu'on voit sur l'entreprise à droite dans google quand on tape \"on vous déménage\" dans la recherche - bonjour@onvoudemenage.ch\n    </div>\n    <div>\n        <a href=\"https://adwords.google.com/aw/overview?ocid=147889933&__c=4334118117&authuser=0&__u=3371461197\" target=\"_blank\">Google AdWords</a> Outil d'achat de mot clé pour la recherche google - bonjour@onvoudemenage.ch\n    </div>\n    <div>\n        <a href=\"https://login.mailchimp.com\" target=\"_blank\">MailChimp</a> Outil d'envoi des emails automatique -- OnVousDemenage\n    </div>\n    <div>\n        <a href=\"https://admin.typeform.com/login/\" target=\"_blank\">Typeform</a> Outils de gestion du formulaire -- bonjour@onvousdemenage.ch\n    </div>\n    <div>\n        <a href=\"/#/pdf\">Template</a>\n    </div>\n    <h2>Dashboard</h2>\n    <div>\n        <a href=\"https://datastudio.google.com/org//reporting/0B-EzZrEgW-uXSFB4N1VMTmk1UTQ/page/b1AD\" target=\"_blank\">Dashboard Analytics</a>\n    </div>\n\n</div>"

/***/ }),

/***/ "./public/app/home/home.component.ts":
/*!*******************************************!*\
  !*** ./public/app/home/home.component.ts ***!
  \*******************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
        this.title = 'OVD - Backoffice';
    }
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./home.component.html */ "./public/app/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./public/app/home/home.component.css")]
        })
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./public/environments/environment.ts":
/*!********************************************!*\
  !*** ./public/environments/environment.ts ***!
  \********************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./public/main.ts":
/*!************************!*\
  !*** ./public/main.ts ***!
  \************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./public/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./public/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!******************************!*\
  !*** multi ./public/main.ts ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/public/web/public/main.ts */"./public/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map