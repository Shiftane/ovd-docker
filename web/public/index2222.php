<!doctype html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <title>OVD - Manager</title>
    <base href="/">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="/node_modules/core-js/client/shim.min.js"></script>

    <script src="/node_modules/zone.js/dist/zone.js"></script>
    <script src="/node_modules/systemjs/dist/system.src.js"></script>
    <script src="./systemjs.config.js"></script>

    <script>
        System.import('./main.js').catch(function(err) {
            console.error(err);
        });
    </script>
    <script src="/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="/node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="./styles.css">

    <link rel="stylesheet" type="text/css" href="/node_modules/bootstrap/dist/css/bootstrap.min.css">


</head>

<body>
    <app-root>
        <div class="full-div">Chargement en cours...</div>
    </app-root>
</body>

</html>