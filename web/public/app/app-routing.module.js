"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var home_component_1 = require("./home/home.component");
var callback_component_1 = require("./auth/callback.component");
var auth_guard_service_1 = require("./auth/auth-guard.service");
var auth_service_1 = require("./auth/auth.service");
var unauthorized_component_1 = require("./auth/unauthorized.component");
var appRoutes = [
    { path: 'home', component: home_component_1.HomeComponent, canActivate: [auth_guard_service_1.AuthGuard] },
    //{ path: 'contract', component:  ContractComponent, canActivate:[AuthGuard]},
    { path: 'login-callback', component: callback_component_1.LoginCallbackComponent },
    { path: '', redirectTo: 'home', pathMatch: 'full', canActivate: [auth_guard_service_1.AuthGuard] },
    { path: 'unauthorized', component: unauthorized_component_1.UnAuhtorizedComponent }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.RouterModule.forRoot(appRoutes),
            ],
            exports: [
                router_1.RouterModule
            ],
            providers: [auth_guard_service_1.AuthGuard, auth_service_1.AuthService],
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=app-routing.module.js.map