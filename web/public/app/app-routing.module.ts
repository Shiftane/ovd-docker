import { NgModule }      from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {LoginCallbackComponent} from './auth/callback.component';
import {AuthGuard} from './auth/auth-guard.service'
import {AuthService} from './auth/auth.service'
import {UnAuhtorizedComponent} from './auth/unauthorized.component'

const appRoutes: Routes = [
  { path: 'home', component:  HomeComponent, canActivate:[AuthGuard]},
  //{ path: 'contract', component:  ContractComponent, canActivate:[AuthGuard]},
  { path: 'login-callback', component:  LoginCallbackComponent},
  { path: '', redirectTo : 'home', pathMatch:'full', canActivate:[AuthGuard]},
  { path: 'unauthorized', component: UnAuhtorizedComponent}    
];


@NgModule({
  imports:      [ 
     RouterModule.forRoot(appRoutes),
     ],
  exports : [
    RouterModule
  ],
  providers:[AuthGuard, AuthService],
})
export class AppRoutingModule { 
  
}