import { Component } from '@angular/core';
import { Router, Event as RouterEvent, NavigationStart, NavigationCancel, NavigationError, NavigationEnd } from '@angular/router';
import { Http } from '@angular/http';
import { ContractService } from './contract/service/contract.service';
import { Contract } from './contract/contract';
import { AuthService } from "./auth/auth.service";



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ContractService, AuthService]
})
export class AppComponent {
  title = 'OVD - Manager';
  constructor(private router : Router, private authService:AuthService, private contractService:ContractService){
    //authService.handleAuthentication();
  };
  
  search(searchTerm : string){
    this.router.navigate(["contract/search/"+searchTerm]);
  }

  login(){
    this.authService.login();
  }
}
