import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContractModule } from './contract/contract.module'
import { HomeComponent } from './home/home.component';
import { LoginCallbackComponent } from './auth/callback.component';
import {UnAuhtorizedComponent} from './auth/unauthorized.component'

@NgModule({
  declarations: [
    //MenuComponent,
    AppComponent,
    HomeComponent,
    LoginCallbackComponent,
    UnAuhtorizedComponent
  
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule, 
    AppRoutingModule,
    ContractModule,

     
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents:[]
})
export class AppModule { }
