import { Component, Input, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';

import { Contract } from '../contract';
import {BasicInfo} from '../contract-basic-info/basic-info';

@Component({
  selector: 'contract-detail',
  templateUrl: './contract-detail.component.html',
  styleUrls: ['./contract-detail.component.css']
})




export class ContractDetailComponent implements OnInit {
  @Input() contract:Contract;
  ngOnInit(): void {
  }

}