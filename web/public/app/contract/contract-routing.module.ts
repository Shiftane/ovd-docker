import { NgModule }      from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContractListComponent } from './contract-list/contract-list.component';
import {ContractDetailComponent } from './contract-detail/contract-detail.component';
import {AuthGuard } from '../auth/auth-guard.service';

const mappingRoutes: Routes = [
  { path: 'contract', component:  ContractListComponent, canActivate:[AuthGuard]},
  { path: 'contract/:id', component:  ContractListComponent},
  { path: 'contract/search/:searchTerm', component:  ContractListComponent},
];


@NgModule({
  imports:      [ 
     RouterModule.forChild(mappingRoutes),
     ],
  exports : [
    RouterModule
  ],
  providers:[AuthGuard]
})
export class ContractRoutingModule { 
  
}