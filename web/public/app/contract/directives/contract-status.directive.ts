import { Directive, ElementRef, Input, OnInit, Renderer} from '@angular/core';
import {Workpackage} from '../contract'
import {ContractService} from '../service/contract.service'

@Directive({ 
    selector: '[contractStatus]',
    providers : [ContractService]
})

export class ContractStatusDirective implements OnInit {
    @Input() contractStatus : Workpackage;
    elementRef: ElementRef;
    
    constructor(public el: ElementRef, public renderer: Renderer, public contractService : ContractService) {
    }
    ngOnInit() {
        switch(this.contractService.getLastStatus(this.contractStatus)){
            case 'TODO':
                this.el.nativeElement.classList.add('bg-warning');
                break;
            case 'TO SEND':
                this.el.nativeElement.classList.add('bg-warning');
                break;
            case 'IN PROGRESS':
                this.el.nativeElement.classList.add('bg-warning');
                break;
            case 'WAITING_TRUE':
                this.el.nativeElement.classList.add('bg-warning');    
                break;
            case 'DONE':
                this.el.nativeElement.classList.add('bg-success');
                break;
            case 'NOT APPLICABLE':
                this.el.nativeElement.classList.add('bg-success');
                break;
            default : 
                this.el.nativeElement.classList.add('bg-danger');
                break;
        }         
    }


    
}