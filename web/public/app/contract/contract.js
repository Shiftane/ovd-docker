"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Contract = /** @class */ (function () {
    function Contract(id, questionsAnswers, workpackages, basicInfo) {
        this.id = id;
        this.questionsAnswers = questionsAnswers;
        this.workpackages = workpackages;
        this.basicInfo = basicInfo;
    }
    return Contract;
    function Contract(id, questionsAnswers) {
        this.id = id;
        this.questionsAnswers = questionsAnswers;
    }
    return Contract;
}());
exports.Contract = Contract;

var Question = /** @class */ (function () {
    function Question(field, answer, mapping) {
        this.field = question;
        this.answer = answer;
        this.mapping = mapping;
    }
    return Question;
}());
exports.Question = Question;

var Workpackage = /** @class */ (function () {
    function Workpackage(id, name, neededFields, sortingPriority, to_print, statuses, actions, lastStatus) {
        this.id = id;
        this.name = name;
        this.neededFields = neededFields;
        this.sortingPriority = sortingPriority;
        this.to_print = to_print;
        this.statuses = statuses;
        this.actions = actions;
        this.lastStatus = lastStatus;
    }
    return Workpackage;
}());
exports.Workpackage = Workpackage;
var Status = /** @class */ (function () {
    function Status(comments, creation_date, form_id, status, status_id, work_package_id) {
        this.comments = comments;
        this.creation_date = creation_date;
        this.form_id = form_id;
        this.status = status;
        this.status_id = status_id;
        this.work_package_id = work_package_id;
    }
    return Status;
}());
exports.Status = Status;
var Action = /** @class */ (function () {
    function Action(action, action_name, comments, condition_attribut, condition_value, id, data, type, work_package_fk) {
        this.action = action;
        this.action_name = action_name;
        this.comments = comments;
        this.condition_attribut = condition_attribut;
        this.condition_value = condition_value;
        this.id = id;
        this.data = data;
        this.type = type;
        this.work_package_fk = work_package_fk;
    }
    return Action;
}());
exports.Action = Action;
//# sourceMappingURL=contract.js.map