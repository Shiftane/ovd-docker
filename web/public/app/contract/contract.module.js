"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var contract_list_component_1 = require("./contract-list/contract-list.component");
var contract_routing_module_1 = require("./contract-routing.module");
var contract_service_1 = require("./service/contract.service");
var contract_detail_component_1 = require("./contract-detail/contract-detail.component");
var contract_basic_info_component_1 = require("./contract-basic-info/contract-basic-info.component");
var findValue_pipe_1 = require("./pipe/findValue.pipe");
var contract_workpackage_list_component_1 = require("./contract-workpackages/contract-workpackage-list.component");
var show_answers_component_1 = require("./contract-workpackages/show-answers.component");
var show_status_component_1 = require("./contract-workpackages/show-status.component");
var show_actions_component_1 = require("./contract-workpackages/show-actions.component");
var add_status_form_component_1 = require("./contract-workpackages/add-status-form.component");
var orderby_pipe_1 = require("./pipe/orderby.pipe");
var contract_status_directive_1 = require("./directives/contract-status.directive");
var ContractModule = /** @class */ (function () {
    function ContractModule() {
    }
    ContractModule = __decorate([
        core_1.NgModule({
            imports: [
                forms_1.FormsModule, common_1.CommonModule, contract_routing_module_1.ContractRoutingModule
            ],
            declarations: [contract_list_component_1.ContractListComponent, contract_detail_component_1.ContractDetailComponent,
                contract_basic_info_component_1.ContractBasicInfoComponent, contract_workpackage_list_component_1.ContractWorkpackageList, show_answers_component_1.ShowAnswersComponent,
                show_actions_component_1.ShowActionsComponent, show_status_component_1.ShowStatusComponent, add_status_form_component_1.AddStatusFormComponent, findValue_pipe_1.FindValueByName, orderby_pipe_1.OrderByPipe, contract_status_directive_1.ContractStatusDirective],
            providers: [contract_service_1.ContractService]
        })
    ], ContractModule);
    return ContractModule;
}());
exports.ContractModule = ContractModule;
//# sourceMappingURL=contract.module.js.map