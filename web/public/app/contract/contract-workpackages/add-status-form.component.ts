import { Component, Input, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Question, Workpackage } from '../contract';
import {BasicInfo} from '../contract-basic-info/basic-info'

import { FindValueByName } from '../pipe/findValue.pipe';
import {Status} from '../contract'


@Component({
  selector: 'add-status-form',
  template: `
            <div class="card">
            {{diagnostic}}
              <form #statusForm="ngForm">
                <div class="form-row">
                  <div class="col-md-8">
                    <textarea class="form-control form-control-sm" id="comments" rows="1" [(ngModel)]="modelStatus.comments" name="comments"></textarea>
                  </div>
                  <div class="col-md-3">
                    <select class="form-control form-control-sm" name="status" [(ngModel)]="modelStatus.status" id="status">
                      <option>TODO</option>
                      <option>TEST</option>
                    </select>
                  </div>
                  <div class="col-md-1">
                    <button type="submit" class="btn btn-primary btn-sm">Sauver</button>
                  </div>
                </div>
              </form>
            </div>
            `,
  providers : [FindValueByName]
})


export class AddStatusFormComponent implements OnInit  {
  @Input() workpackage:Workpackage;
  private modelStatus : Status;
  constructor(private findValueByNamePipe : FindValueByName){
  };

  ngOnInit(){
    this.modelStatus = new Status("",new Date(),"","",0,this.workpackage.id);
  }
  addStatus(){
    
  }
  get diagnostic() { return JSON.stringify(this.modelStatus); }
  
}