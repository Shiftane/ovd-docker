import { Component, Input, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';

import { Contract, Workpackage } from '../contract';
import {BasicInfo} from '../contract-basic-info/basic-info'

import { FindValueByName } from '../pipe/findValue.pipe';

@Component({
  selector: 'contract-workpackage-list',
  templateUrl: './contract-workpackage-list.component.html',
  styleUrls: ['./contract-workpackage-list.component.css'],
  providers : [FindValueByName],
})




export class ContractWorkpackageList implements OnInit {
  @Input() contract:Contract;
  selectedWorkpackage:Workpackage;
  ngOnInit(): void {
  }
  constructor(private findValueByNamePipe : FindValueByName){};
  
  selectWorkPackage(workpackage:Workpackage): void {
    this.selectedWorkpackage = workpackage;
  }
  
}