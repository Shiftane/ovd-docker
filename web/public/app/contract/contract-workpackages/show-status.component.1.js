"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
// Observable operators
require("rxjs/add/operator/debounceTime");
require("rxjs/add/operator/distinctUntilChanged");
require("rxjs/add/operator/switchMap");
require("rxjs/add/operator/catch");
// Observable class extensions
require("rxjs/add/observable/of");
var contract_1 = require("../contract");
var findValue_pipe_1 = require("../pipe/findValue.pipe");
var ShowStatusComponent = (function () {
    function ShowStatusComponent(findValueByNamePipe) {
        this.findValueByNamePipe = findValueByNamePipe;
    }
    ;
    return ShowStatusComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", contract_1.Workpackage)
], ShowStatusComponent.prototype, "workpackage", void 0);
ShowStatusComponent = __decorate([
    core_1.Component({
        selector: 'show-status',
        template: "\n            <div class=\"card\">\n              <div class=\"card-header\">Historique</div>\n              <div *ngIf=\"workpackage.statuses.length < 1\">Pas de commentaire</div>\n              <ul class=\"items card-body\">\n              <li class=\"row\" *ngFor=\"let status of workpackage.statuses\">\n                  <div class=\"col-md-3\">{{status.creation_date}}</div>\n                  <div class=\"col-md-6\">{{status.comments}}</div>\n                  <div class=\"col-md-3\">{{status.status}}</div>\n              </li>\n              </ul>\n            </div>\n            ",
        providers: [findValue_pipe_1.FindValueByName]
    }),
    __metadata("design:paramtypes", [findValue_pipe_1.FindValueByName])
], ShowStatusComponent);
exports.ShowStatusComponent = ShowStatusComponent;
//# sourceMappingURL=show-status.component.1.js.map