import { Component, Input, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Question, Workpackage, Action } from '../contract';
import {BasicInfo} from '../contract-basic-info/basic-info'
import {ContractService} from '../service/contract.service'

import { FindValueByName } from '../pipe/findValue.pipe';


@Component({
  selector: 'show-actions',
  template: `<div class="card">
              <div class="card-header">Actions possible</div>
              <ul class="items card-body">
              <li class="row" *ngFor="let action of workpackage.actions">
                  
                  <div class="col-md-12">
                    <button class="btn btn-primary" type="submit" (click)="fireAction(action)">{{action.action_name}}</button>
                  </div>

                  <div class="col-md-12"><b>Que faire : </b> {{action.comments}}</div>
              </li>
              </ul>
            </div>
              `,
  providers : [FindValueByName]
})


export class ShowActionsComponent{
  @Input() workpackage:Workpackage;
  
  constructor(private findValueByNamePipe : FindValueByName,
    public contractService : ContractService){};
  public fireAction(action:Action){
    switch(action.type){
      case "EXTERNAL_LINK":
        this.openExternalLink(action);
        break;
      default:
    }
  } 

  openExternalLink(action : Action){
    //var url = this.contractService.replaceVarByValue(action.action, );
    var newWindow = window.open('some_url');        
  }
}
