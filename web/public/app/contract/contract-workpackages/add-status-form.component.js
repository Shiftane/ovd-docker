"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var contract_1 = require("../contract");
var findValue_pipe_1 = require("../pipe/findValue.pipe");
var contract_2 = require("../contract");
var AddStatusFormComponent = /** @class */ (function () {
    function AddStatusFormComponent(findValueByNamePipe) {
        this.findValueByNamePipe = findValueByNamePipe;
    }
    ;
    AddStatusFormComponent.prototype.ngOnInit = function () {
        this.modelStatus = new contract_2.Status("", new Date(), "", "", 0, this.workpackage.id);
    };
    AddStatusFormComponent.prototype.addStatus = function () {
    };
    Object.defineProperty(AddStatusFormComponent.prototype, "diagnostic", {
        get: function () { return JSON.stringify(this.modelStatus); },
        enumerable: true,
        configurable: true
    });
    __decorate([
        core_1.Input(),
        __metadata("design:type", contract_1.Workpackage)
    ], AddStatusFormComponent.prototype, "workpackage", void 0);
    AddStatusFormComponent = __decorate([
        core_1.Component({
            selector: 'add-status-form',
            template: "\n            <div class=\"card\">\n            {{diagnostic}}\n              <form #statusForm=\"ngForm\">\n                <div class=\"form-row\">\n                  <div class=\"col-md-8\">\n                    <textarea class=\"form-control form-control-sm\" id=\"comments\" rows=\"1\" [(ngModel)]=\"modelStatus.comments\" name=\"comments\"></textarea>\n                  </div>\n                  <div class=\"col-md-3\">\n                    <select class=\"form-control form-control-sm\" name=\"status\" [(ngModel)]=\"modelStatus.status\" id=\"status\">\n                      <option>TODO</option>\n                      <option>TEST</option>\n                    </select>\n                  </div>\n                  <div class=\"col-md-1\">\n                    <button type=\"submit\" class=\"btn btn-primary btn-sm\">Sauver</button>\n                  </div>\n                </div>\n              </form>\n            </div>\n            ",
            providers: [findValue_pipe_1.FindValueByName]
        }),
        __metadata("design:paramtypes", [findValue_pipe_1.FindValueByName])
    ], AddStatusFormComponent);
    return AddStatusFormComponent;
}());
exports.AddStatusFormComponent = AddStatusFormComponent;
//# sourceMappingURL=add-status-form.component.js.map