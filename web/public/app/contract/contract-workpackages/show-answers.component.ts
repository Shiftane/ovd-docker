import { Component, Input, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';

import { Question } from '../contract';
import {BasicInfo} from '../contract-basic-info/basic-info'

import { FindValueByName } from '../pipe/findValue.pipe';


@Component({
  selector: 'show-answers',
  template: `<div class="card">
              <div class="card-header">Information sur le client</div>
              <ul class="items card-body">
              <li class="row" *ngFor="let field of getFields()">
                  <div class="col-md-6">{{field.q}}</div>
                  <div class="col-md-6">{{field.a}}</div>
              </li>
              </ul>
            </div>
              `,
  providers : [FindValueByName]
})




export class ShowAnswersComponent{
  @Input() answers:Question[];
  @Input() fieldsToShow:string;
  getFields(){
      let fields = this.fieldsToShow.split(',');
      let fieldsRet: {q:string, a:string}[] = [];
      for (var index = 0; index < fields.length; index++) {
        let field = fields[index];
        let q = this.getQuestion(field);
        let a = this.getAnswer(field);
        if(q && a != null && q != null){
            fieldsRet.push({'q':q, 'a':a});
        }
      }
      
      return fieldsRet;
  }
  constructor(private findValueByNamePipe : FindValueByName){};
  getQuestion(field : String){
    if(field === ""){
      return null;
    }
    let answer = this.answers.filter(answer => answer.mapping && answer.mapping.toLowerCase() === field.toLowerCase())
    return answer && answer.length > 0 && answer[0].question;
  }
  getAnswer(field:String){
    return this.findValueByNamePipe.transform(this.answers,field);
  }
}