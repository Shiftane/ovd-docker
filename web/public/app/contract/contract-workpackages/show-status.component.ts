import { Component, Input, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Question, Workpackage } from '../contract';
import {BasicInfo} from '../contract-basic-info/basic-info'

import { FindValueByName } from '../pipe/findValue.pipe';


@Component({
  selector: 'show-status',
  template: `
            <div class="card">
              <div class="card-header">Historique</div>
              <div *ngIf="workpackage.statuses.length < 1">Pas de commentaire</div>
              <ul class="items card-body">
              <li class="row" *ngFor="let status of workpackage.statuses">
                  <div class="col-md-3">{{status.creation_date}}</div>
                  <div class="col-md-6">{{status.comments}}</div>
                  <div class="col-md-3">{{status.status}}</div>
              </li>
              </ul>
            </div>
            `,
  providers : [FindValueByName]
})


export class ShowStatusComponent{
  @Input() workpackage:Workpackage;
  
  constructor(private findValueByNamePipe : FindValueByName){};
  
}