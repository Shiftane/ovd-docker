"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var findValue_pipe_1 = require("../pipe/findValue.pipe");
var ShowAnswersComponent = /** @class */ (function () {
    function ShowAnswersComponent(findValueByNamePipe) {
        this.findValueByNamePipe = findValueByNamePipe;
    }
    ShowAnswersComponent.prototype.getFields = function () {
        var fields = this.fieldsToShow.split(',');
        var fieldsRet = [];
        for (var index = 0; index < fields.length; index++) {
            var field = fields[index];
            var q = this.getQuestion(field);
            var a = this.getAnswer(field);
            if (q && a != null && q != null) {
                fieldsRet.push({ 'q': q, 'a': a });
            }
        }
        return fieldsRet;
    };
    ;
    ShowAnswersComponent.prototype.getQuestion = function (field) {
        if (field === "") {
            return null;
        }
        var answer = this.answers.filter(function (answer) { return answer.mapping && answer.mapping.toLowerCase() === field.toLowerCase(); });
        return answer && answer.length > 0 && answer[0].question;
    };
    ShowAnswersComponent.prototype.getAnswer = function (field) {
        return this.findValueByNamePipe.transform(this.answers, field);
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Array)
    ], ShowAnswersComponent.prototype, "answers", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], ShowAnswersComponent.prototype, "fieldsToShow", void 0);
    ShowAnswersComponent = __decorate([
        core_1.Component({
            selector: 'show-answers',
            template: "<div class=\"card\">\n              <div class=\"card-header\">Information sur le client</div>\n              <ul class=\"items card-body\">\n              <li class=\"row\" *ngFor=\"let field of getFields()\">\n                  <div class=\"col-md-6\">{{field.q}}</div>\n                  <div class=\"col-md-6\">{{field.a}}</div>\n              </li>\n              </ul>\n            </div>\n              ",
            providers: [findValue_pipe_1.FindValueByName]
        }),
        __metadata("design:paramtypes", [findValue_pipe_1.FindValueByName])
    ], ShowAnswersComponent);
    return ShowAnswersComponent;
}());
exports.ShowAnswersComponent = ShowAnswersComponent;
//# sourceMappingURL=show-answers.component.js.map