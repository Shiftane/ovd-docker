import {BasicInfo} from './contract-basic-info/basic-info'




export class Contract{
    constructor(
        public id:string,
        public questionsAnswers:Question[],
        public workpackages:Workpackage[],
        public basicInfo: BasicInfo
        ){}
        
}

export class Question{
    constructor(public question: string,
    public answer:string, public mapping:string){}
}

export class Workpackage{
    constructor(
        public id : number,
        public name : string,
        public neededFields : string,
        public sortingPriority : number,
        public to_print : boolean,
        public statuses : Status[],
        public actions : Action[],
        public lastStatus : string,
    ){}
}

export class Status{
    constructor(
        public comments : string,
        public creation_date : Date,
        public form_id : string,
        public status : string,
        public status_id : number,
        public work_package_id:number
    ){}
}

export class Action{
    constructor(
        public action : string,
        public action_name : string,
        public comments : string,
        public condition_attribut : string,
        public condition_value : number,
        public id:number,
        public data:string,
        public type:string,
        public work_package_fk:number
    ){}
}