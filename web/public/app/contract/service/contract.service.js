"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
require("rxjs/add/operator/catch");
require("rxjs/add/operator/map");
var ContractService = /** @class */ (function () {
    function ContractService(http) {
        this.http = http;
        this.headers = new http_1.Headers({
            'X-API-KEY': '612e648bf9594adb50844cad6895f2cf',
        });
        this.mappingsUrl = '/api/v2/contracts'; // URL to web API
    }
    ContractService.prototype.updateContracts = function () {
        localStorage.removeItem("contracts");
        this.getContracts();
    };
    ContractService.prototype.getContracts = function () {
        var _this = this;
        var storageContracts = JSON.parse(localStorage.getItem("contracts"));
        if (typeof (storageContracts) != 'undefined' && storageContracts != null && storageContracts.length > 0) {
            return Promise.resolve(storageContracts);
        }
        return this.http.get(this.mappingsUrl, { headers: this.headers })
            .toPromise()
            .then(function (response) {
            var contracts = response.json();
            contracts = _this.populateBasicInfo(contracts);
            localStorage.setItem("contracts", JSON.stringify(contracts));
            return contracts;
            //return response.json() as Contract[];
        });
    };
    ContractService.prototype.getContract = function (id) {
        var storageContracts = JSON.parse(localStorage.getItem("contracts"));
        if (typeof (storageContracts) != 'undefined' && storageContracts != null && storageContracts.length > 0) {
            return storageContracts.find(function (contract) { return contract.id === id; });
        }
        return this.getContracts().then(function (contracts) { return contracts.find(function (contract) { return contract.id === id; }); });
    };
    ContractService.prototype.getValueForName = function (allValues, name) {
        var questionsFiltered = allValues.filter(function (question) { return question.mapping && name && question.mapping.toLowerCase() == name.toLowerCase(); });
        if (questionsFiltered.length == 0) {
            return "[VALUE NOT FOUND : " + name + "]";
        }
        return questionsFiltered[0].answer;
    };
    ContractService.prototype.handleError = function (error) {
        // In a real world app, you might use a remote logging infrastructure
        var errMsg;
        if (error instanceof http_1.Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Promise.call(errMsg);
    };
    ContractService.prototype.populateBasicInfo = function (contracts) {
        for (var index = 0; index < contracts.length; index++) {
            if (contract.questionsAnswers != null) {
                var contract = contracts[index];
                /* contract.basicInfo = new BasicInfo(
                             this.getValueForName(contract.questionsAnswers, "name"),
                             this.getValueForName(contract.questionsAnswers, "firstname"),
                             this.getValueForName(contract.questionsAnswers, "phoneNumber"),
                             this.getValueForName(contract.questionsAnswers, "email"),
                             new Date(this.getValueForName(contract.questionsAnswers, "birthdate")),
                             this.getValueForName(contract.questionsAnswers, "howToContactCustomer"),
                             this.getValueForName(contract.questionsAnswers, "street"),
                             this.getValueForName(contract.questionsAnswers, "city"),
                             this.getValueForName(contract.questionsAnswers, "Floor"),
                             new Date(this.getValueForName(contract.questionsAnswers, "movingDate")),
                             this.getValueForName(contract.questionsAnswers, "contactType"),
                             this.getValueForName(contract.questionsAnswers, "newStreet"),
                             this.getValueForName(contract.questionsAnswers, "newCity"),
                             this.getValueForName(contract.questionsAnswers, "NewFloor"),
                             this.getStatus(contract.questionsAnswers, contract.workpackages),
                             new Date(this.getValueForName(contract.questionsAnswers, "submitDate")),
                 );*/
            }
        }
        return contracts;
    };
    ContractService.prototype.getStatus = function (questions, workpackages) {
        var status = { workpackageDone: 0, workpackageInProgress: 0, workpackageTodo: 0 };
        for (var index = 0; index < workpackages.length; index++) {
            var workpackage = workpackages[index];
            workpackage.lastStatus = this.getLastStatus(workpackage);
            if (workpackage.lastStatus == null) {
                status.workpackageTodo++;
            }
            else {
                switch (workpackage.lastStatus) {
                    case "DONE":
                        status.workpackageDone++;
                        break;
                    case "IN PROGRESS":
                        status.workpackageInProgress++;
                        break;
                    case "WAITING_TRUE":
                        status.workpackageInProgress++;
                        break;
                    case "TO SEND":
                        status.workpackageInProgress++;
                        break;
                    case "TO_PRINT":
                        status.workpackageInProgress++;
                        break;
                    case "NOT APPLICABLE": break;
                    default:
                        status.workpackageTodo++;
                        break;
                }
            }
        }
        return status;
    };
    ContractService.prototype.getLastStatus = function (workpackage) {
        if (workpackage.statuses.length > 0) {
            workpackage.statuses.sort(function (a, b) {
                if (a.creation_date < b.creation_date) {
                    return 1;
                }
                else if (a.creation_date > b.creation_date) {
                    return -1;
                }
                else {
                    return 0;
                }
            });
            return workpackage.statuses[0].status;
        }
        return null;
    };
    ContractService.prototype.replaceVarByValue = function (strValue, answers) {
        var regExp = /{{[^}]*}}/;
        var dataVar = regExp.exec(strValue);
        var filledData = strValue;
        while (dataVar != null && dataVar.length > 0) {
            var varToReplace = dataVar[0].replace("{{", "").replace("}}", "");
            var valueFounded = this.getValue(answers, varToReplace);
            if (valueFounded.length > 0) {
                filledData = filledData.replace(dataVar[0], valueFounded[0].answer);
            }
            else {
                console.error("No answer founded for " + varToReplace);
                filledData = filledData.replace(dataVar[0], "ERROR: " + varToReplace);
                break;
            }
            dataVar = regExp.exec(filledData);
        }
        return filledData;
    };
    ContractService.prototype.getValue = function (scope, name) {
        return scope.filter(function (value) { return value.answer === name; });
    };
    ;
    ContractService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http])
    ], ContractService);
    return ContractService;
}());
exports.ContractService = ContractService;
//# sourceMappingURL=contract.service.js.map