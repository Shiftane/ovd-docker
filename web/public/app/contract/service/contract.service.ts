import { Injectable } from '@angular/core';
import { Http, Response, Headers }          from '@angular/http';

import { Contract, Question, Workpackage, Status } from '../contract';
import {BasicInfo} from '../contract-basic-info/basic-info'

@Injectable()
export class ContractService {
  private headers = new Headers({
    'X-API-KEY':'612e648bf9594adb50844cad6895f2cf'
    });
  private mappingsUrl = 'http://localhost:8000/api/v2/contracts';  // URL to web API
  private _cachedResult : Promise<Contract[]>;
  constructor (private http: Http) {
   
  }
  updateContracts(): void {
    localStorage.removeItem("contracts");
    this.getContracts();
  }

  getContracts(): Promise<Contract[]> {
    
    var storageContracts = JSON.parse(localStorage.getItem("contracts"));
    
    if(typeof(storageContracts) != 'undefined' && storageContracts != null && storageContracts.length > 0){
      return Promise.resolve(storageContracts);
    }
    return this.http.get(this.mappingsUrl, {headers: this.headers})
                                .toPromise()
                                .then(response => {
                                  
                                  let contracts = response.json() as Contract[]
                                  contracts = this.populateBasicInfo(contracts);
                                  localStorage.setItem("contracts",  JSON.stringify(contracts));
                                  return contracts;
                                  //return response.json() as Contract[];
                                  })
                                //.catch((res: Response) => this.handleError(res))
                                ;
    
  }

  getContract(id : string): Promise<Contract>{
    var storageContracts = JSON.parse(localStorage.getItem("contracts"));
    if(typeof(storageContracts) != 'undefined' && storageContracts != null && storageContracts.length > 0){
      return storageContracts.find(contract => contract.id === id)
    }
    return this.getContracts().then(contracts => contracts.find(contract => contract.id === id));
  }

  getValueForName(allValues: Question[], name: String ) {
    
    let questionsFiltered:Question[] = allValues.filter(question => question.mapping && name && question.mapping.toLowerCase()==name.toLowerCase());
    if(questionsFiltered.length == 0){
        return "[VALUE NOT FOUND : "+name+"]";
    }

    return questionsFiltered[0].answer;
  }

  private handleError (error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Promise.call(errMsg);
  }

   populateBasicInfo(contracts : Contract[]){
    for (var index = 0; index < contracts.length; index++) {
      if(contract.questionsAnswers != null){
        var contract = contracts[index];
       /* contract.basicInfo = new BasicInfo(
                    this.getValueForName(contract.questionsAnswers, "name"),
                    this.getValueForName(contract.questionsAnswers, "firstname"),
                    this.getValueForName(contract.questionsAnswers, "phoneNumber"),
                    this.getValueForName(contract.questionsAnswers, "email"),
                    new Date(this.getValueForName(contract.questionsAnswers, "birthdate")),
                    this.getValueForName(contract.questionsAnswers, "howToContactCustomer"),
                    this.getValueForName(contract.questionsAnswers, "street"),
                    this.getValueForName(contract.questionsAnswers, "city"),
                    this.getValueForName(contract.questionsAnswers, "Floor"),
                    new Date(this.getValueForName(contract.questionsAnswers, "movingDate")),
                    this.getValueForName(contract.questionsAnswers, "contactType"),
                    this.getValueForName(contract.questionsAnswers, "newStreet"),
                    this.getValueForName(contract.questionsAnswers, "newCity"),
                    this.getValueForName(contract.questionsAnswers, "NewFloor"),
                    this.getStatus(contract.questionsAnswers, contract.workpackages),
                    new Date(this.getValueForName(contract.questionsAnswers, "submitDate")),
        );*/
      }
    }
    return contracts;
  }

  private getStatus(questions: Question[], workpackages: Workpackage[] ){
    let status = {workpackageDone : 0, workpackageInProgress : 0, workpackageTodo : 0};
    for (var index = 0; index < workpackages.length; index++) {
      var workpackage = workpackages[index];
      workpackage.lastStatus = this.getLastStatus(workpackage);
      if(workpackage.lastStatus == null){
        status.workpackageTodo++;
      }else{
        switch (workpackage.lastStatus) {
          case "DONE": status.workpackageDone++;break;
          case "IN PROGRESS": status.workpackageInProgress++;break;
          case "WAITING_TRUE": status.workpackageInProgress++;break;
          case "TO SEND": status.workpackageInProgress++;break;
          case "TO_PRINT": status.workpackageInProgress++;break;
          case "NOT APPLICABLE": break;
          default:status.workpackageTodo++;break;
        }
      }
    }
    return status;
  }
  getLastStatus(workpackage:Workpackage):string{
    if(workpackage.statuses.length > 0){
      workpackage.statuses.sort((a,b) => {
            if (a.creation_date < b.creation_date) {
                    return 1;
                } else if (a.creation_date > b.creation_date) {
                    return -1;
                } else {
                    return 0;
                }
            });
            return workpackage.statuses[0].status;
    }
    return null;    
  }

  replaceVarByValue(strValue : string, answers : Question[]):string {
    var regExp = /{{[^}]*}}/;
    var dataVar = regExp.exec(strValue);
    var filledData = strValue;
    while (dataVar != null && dataVar.length > 0) {
        var varToReplace = dataVar[0].replace("{{", "").replace("}}", "")
        var valueFounded = this.getValue(answers, varToReplace);
        if (valueFounded.length > 0) {
            filledData = filledData.replace(dataVar[0], valueFounded[0].answer);
        } else {
            console.error("No answer founded for " + varToReplace);
            filledData = filledData.replace(dataVar[0], "ERROR: " + varToReplace);
            break;
        }
        dataVar = regExp.exec(filledData);
    }
    return filledData;
}
  getValue (scope : Question[], name : string) : Question[] {
    return scope.filter( value => value.answer === name );
  };
}