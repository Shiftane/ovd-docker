export class BasicInfo{
    constructor(
        public name:string,
        public firstname:string,
        public phoneNumber:string,
        public email:string,
        public birthDate:Date,
        public contactMethod:string,
        public street:string,
        public city:string,
        public floor:string,
        public movingDate:Date,
        public sendMethod:string,
        public newStreet:string,
        public newCity:string,
        public newFloor:string,
        public status:Status,
        public submitDate:Date,
        ){}
}

export class Status{
    constructor(
        public workpackageTodo:number,    
        public workpackageInProgress:number,
        public workpackageDone:number,
    ){}
}
