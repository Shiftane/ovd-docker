import { Component, Input, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Contract } from '../contract';
import { BasicInfo } from '../contract-basic-info/basic-info';

@Component({
  selector: 'contract-basic-info',
  templateUrl: './contract-basic-info.component.html',
  styleUrls: ['./contract-basic-info.component.css']
})




export class ContractBasicInfoComponent implements OnInit {
  @Input("basic-info") basicInfo:BasicInfo;
  ngOnInit(): void {
  }
 
}