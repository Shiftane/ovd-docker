"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var findValue_pipe_1 = require("../pipe/findValue.pipe");
var orderby_pipe_1 = require("../pipe/orderby.pipe");
var contract_service_1 = require("../service/contract.service");
var ContractListComponent = /** @class */ (function () {
    function ContractListComponent(contractService, findValueByNamePipe, route, router) {
        this.contractService = contractService;
        this.findValueByNamePipe = findValueByNamePipe;
        this.route = route;
        this.router = router;
        this.loading = true;
        this.loadingMessage = "Chargement des contracts en cours...";
    }
    ;
    ContractListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var contractsPromise = this.contractService.getContracts();
        contractsPromise.then(function (contracts) { _this.loading = false; _this.contracts = contracts; });
        this.route.params.map(function (params) { return params["id"] && _this.contractService.getContract(params["id"]).then(function (contract) { return _this.selectedContract = contract; }); });
    };
    ContractListComponent.prototype.onSelect = function (contract) {
        this.selectedContract = contract;
    };
    ContractListComponent.prototype.filterByName = function (contract, term) {
        var nameValue = this.findValueByNamePipe.transform(contract.questionsAnswers, 'Name').toLowerCase();
        var firstNameValue = this.findValueByNamePipe.transform(contract.questionsAnswers, 'Firstname').toLowerCase();
        return firstNameValue.indexOf(term.toLowerCase()) > -1 || nameValue.indexOf(term.toLowerCase()) > -1;
    };
    ContractListComponent.prototype.search = function (term) {
    };
    ContractListComponent.prototype.getSize = function (status, refNumber) {
        var total = status.workpackageInProgress + status.workpackageDone + status.workpackageTodo;
        var percent = Math.round(100 * (refNumber / total));
        return { 'width': percent + '%' };
    };
    ContractListComponent = __decorate([
        core_1.Component({
            selector: 'contract-list',
            templateUrl: './contract-list.component.html',
            styleUrls: ['./contract-list.component.css'],
            providers: [contract_service_1.ContractService, findValue_pipe_1.FindValueByName, orderby_pipe_1.OrderByPipe]
        }),
        __metadata("design:paramtypes", [contract_service_1.ContractService,
            findValue_pipe_1.FindValueByName,
            router_1.ActivatedRoute,
            router_1.Router])
    ], ContractListComponent);
    return ContractListComponent;
}());
exports.ContractListComponent = ContractListComponent;
//# sourceMappingURL=contract-list.component.js.map