import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Router,ActivatedRoute, Params } from '@angular/router';

import { FindValueByName } from '../pipe/findValue.pipe';
import {OrderByPipe} from '../pipe/orderby.pipe';


import { Contract } from '../contract';
import { ContractService } from '../service/contract.service';
import {Status} from '../contract-basic-info/basic-info';



@Component({
  selector: 'contract-list',
  templateUrl: './contract-list.component.html',
  styleUrls: ['./contract-list.component.css'],
  providers: [ContractService, FindValueByName, OrderByPipe]
})




export class ContractListComponent implements OnInit {

  loading: boolean = true;

  constructor(
    private contractService: ContractService, 
    private findValueByNamePipe : FindValueByName ,  
    private route: ActivatedRoute, 
    private router: Router){};
  contracts : Contract[];
  selectedContract : Contract;
  loadingMessage = "Chargement des contracts en cours..."

  ngOnInit(): void {
    let contractsPromise = this.contractService.getContracts();
    contractsPromise.then(contracts => {this.loading = false;this.contracts = contracts});
    
    this.route.params.subscribe(params => params["id"] && this.contractService.getContract(params["id"]).then(contract => this.selectedContract = contract));
    
  }
  onSelect(contract : Contract){
    this.selectedContract = contract;
  }
  filterByName(contract: Contract, term:string){
    let nameValue = this.findValueByNamePipe.transform(contract.questionsAnswers, 'Name').toLowerCase();
    let firstNameValue = this.findValueByNamePipe.transform(contract.questionsAnswers, 'Firstname').toLowerCase();
    return firstNameValue.indexOf(term.toLowerCase()) > -1 || nameValue.indexOf(term.toLowerCase()) > -1;
  }
  search(term:string): void{
  }
  getSize(status:Status, refNumber:number){
    let total = status.workpackageInProgress + status.workpackageDone + status.workpackageTodo;
    let percent = Math.round(100*(refNumber/total));
    return {'width':percent+'%'};
  }
}