"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var FindValueByName = /** @class */ (function () {
    function FindValueByName() {
    }
    FindValueByName.prototype.transform = function (allValues, name) {
        var questionsFiltered = allValues.filter(function (question) { return question.mapping && name && question.mapping.toLowerCase() == name.toLowerCase(); });
        if (questionsFiltered.length == 0) {
            return "[VALUE NOT FOUND : " + name + "]";
        }
        return questionsFiltered[0].answer;
    };
    FindValueByName = __decorate([
        core_1.Pipe({ name: 'findValueByName' })
    ], FindValueByName);
    return FindValueByName;
}());
exports.FindValueByName = FindValueByName;
//# sourceMappingURL=findValue.pipe.js.map