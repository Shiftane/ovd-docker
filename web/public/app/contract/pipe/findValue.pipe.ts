import { Pipe, PipeTransform } from '@angular/core';
import { Question } from '../contract';


@Pipe({ name: 'findValueByName' })
export class FindValueByName implements PipeTransform {
  transform(allValues: Question[], name: String ) {
    let questionsFiltered:Question[] = allValues.filter(question => question.mapping && name && question.mapping.toLowerCase()==name.toLowerCase());
    if(questionsFiltered.length == 0){
        return "[VALUE NOT FOUND : "+name+"]";
    }

    return questionsFiltered[0].answer;
  }
}