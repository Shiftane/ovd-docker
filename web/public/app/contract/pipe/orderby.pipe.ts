import {Pipe, PipeTransform} from '@angular/core';
import {Contract} from '../contract'

@Pipe({
 name: 'orderBy'
})
export class OrderByPipe implements PipeTransform{

 transform(array: Contract[], args: string): Contract[] {

  if(!array || array === undefined || array.length === 0) return null;

    array.sort((a: any, b: any) => {
      if (a.basicInfo.movingDate < b.basicInfo.movingDate) {
        return 1;
      } else if (a.basicInfo.movingDate > b.basicInfo.movingDate) {
        return -1;
      } else {
        return 0;
      }
    });
    return array;
  }

}