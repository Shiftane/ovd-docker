import { NgModule }      from '@angular/core';
import { CommonModule }   from '@angular/common';

import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import { ContractListComponent }  from './contract-list/contract-list.component';
import {ContractRoutingModule} from './contract-routing.module';
import {ContractService} from './service/contract.service';
import {ContractDetailComponent} from './contract-detail/contract-detail.component';
import {ContractBasicInfoComponent} from './contract-basic-info/contract-basic-info.component';
import { FindValueByName } from './pipe/findValue.pipe';
import { ContractWorkpackageList } from './contract-workpackages/contract-workpackage-list.component';
import { ShowAnswersComponent } from './contract-workpackages/show-answers.component';
import { ShowStatusComponent } from './contract-workpackages/show-status.component';
import { ShowActionsComponent } from './contract-workpackages/show-actions.component';
import { AddStatusFormComponent } from './contract-workpackages/add-status-form.component';
import {OrderByPipe} from './pipe/orderby.pipe'
import {ContractStatusDirective} from './directives/contract-status.directive'


@NgModule({
  imports:      [ 
     FormsModule, CommonModule, ContractRoutingModule
],
  declarations: [  ContractListComponent, ContractDetailComponent, 
    ContractBasicInfoComponent, ContractWorkpackageList,ShowAnswersComponent, 
    ShowActionsComponent,ShowStatusComponent, AddStatusFormComponent, FindValueByName, OrderByPipe, ContractStatusDirective],
  providers : [ContractService]
})
export class ContractModule { 
  
}