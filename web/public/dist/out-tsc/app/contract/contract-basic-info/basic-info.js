"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BasicInfo = /** @class */ (function () {
    function BasicInfo(name, firstname, phoneNumber, email, birthDate, contactMethod, street, city, floor, movingDate, sendMethod, newStreet, newCity, newFloor, status, submitDate) {
        this.name = name;
        this.firstname = firstname;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.birthDate = birthDate;
        this.contactMethod = contactMethod;
        this.street = street;
        this.city = city;
        this.floor = floor;
        this.movingDate = movingDate;
        this.sendMethod = sendMethod;
        this.newStreet = newStreet;
        this.newCity = newCity;
        this.newFloor = newFloor;
        this.status = status;
        this.submitDate = submitDate;
    }
    return BasicInfo;
}());
exports.BasicInfo = BasicInfo;
var Status = /** @class */ (function () {
    function Status(workpackageTodo, workpackageInProgress, workpackageDone) {
        this.workpackageTodo = workpackageTodo;
        this.workpackageInProgress = workpackageInProgress;
        this.workpackageDone = workpackageDone;
    }
    return Status;
}());
exports.Status = Status;
//# sourceMappingURL=basic-info.js.map