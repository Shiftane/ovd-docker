"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var contract_1 = require("../contract");
var findValue_pipe_1 = require("../pipe/findValue.pipe");
var ContractWorkpackageList = /** @class */ (function () {
    function ContractWorkpackageList(findValueByNamePipe) {
        this.findValueByNamePipe = findValueByNamePipe;
    }
    ContractWorkpackageList.prototype.ngOnInit = function () {
    };
    ;
    ContractWorkpackageList.prototype.selectWorkPackage = function (workpackage) {
        this.selectedWorkpackage = workpackage;
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", contract_1.Contract)
    ], ContractWorkpackageList.prototype, "contract", void 0);
    ContractWorkpackageList = __decorate([
        core_1.Component({
            selector: 'contract-workpackage-list',
            templateUrl: './contract-workpackage-list.component.html',
            styleUrls: ['./contract-workpackage-list.component.css'],
            providers: [findValue_pipe_1.FindValueByName],
        }),
        __metadata("design:paramtypes", [findValue_pipe_1.FindValueByName])
    ], ContractWorkpackageList);
    return ContractWorkpackageList;
}());
exports.ContractWorkpackageList = ContractWorkpackageList;
//# sourceMappingURL=contract-workpackage-list.component.js.map