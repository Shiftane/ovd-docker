"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var contract_1 = require("../contract");
var contract_service_1 = require("../service/contract.service");
var findValue_pipe_1 = require("../pipe/findValue.pipe");
var ShowActionsComponent = /** @class */ (function () {
    function ShowActionsComponent(findValueByNamePipe, contractService) {
        this.findValueByNamePipe = findValueByNamePipe;
        this.contractService = contractService;
    }
    ;
    ShowActionsComponent.prototype.fireAction = function (action) {
        switch (action.type) {
            case "EXTERNAL_LINK":
                this.openExternalLink(action);
                break;
            default:
        }
    };
    ShowActionsComponent.prototype.openExternalLink = function (action) {
        //var url = this.contractService.replaceVarByValue(action.action, );
        var newWindow = window.open('some_url');
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", contract_1.Workpackage)
    ], ShowActionsComponent.prototype, "workpackage", void 0);
    ShowActionsComponent = __decorate([
        core_1.Component({
            selector: 'show-actions',
            template: "<div class=\"card\">\n              <div class=\"card-header\">Actions possible</div>\n              <ul class=\"items card-body\">\n              <li class=\"row\" *ngFor=\"let action of workpackage.actions\">\n                  \n                  <div class=\"col-md-12\">\n                    <button class=\"btn btn-primary\" type=\"submit\" (click)=\"fireAction(action)\">{{action.action_name}}</button>\n                  </div>\n\n                  <div class=\"col-md-12\"><b>Que faire : </b> {{action.comments}}</div>\n              </li>\n              </ul>\n            </div>\n              ",
            providers: [findValue_pipe_1.FindValueByName]
        }),
        __metadata("design:paramtypes", [findValue_pipe_1.FindValueByName,
            contract_service_1.ContractService])
    ], ShowActionsComponent);
    return ShowActionsComponent;
}());
exports.ShowActionsComponent = ShowActionsComponent;
//# sourceMappingURL=show-actions.component.js.map