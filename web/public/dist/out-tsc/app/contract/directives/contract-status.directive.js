"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var contract_1 = require("../contract");
var contract_service_1 = require("../service/contract.service");
var ContractStatusDirective = /** @class */ (function () {
    function ContractStatusDirective(el, renderer, contractService) {
        this.el = el;
        this.renderer = renderer;
        this.contractService = contractService;
    }
    ContractStatusDirective.prototype.ngOnInit = function () {
        switch (this.contractService.getLastStatus(this.contractStatus)) {
            case 'TODO':
                this.el.nativeElement.classList.add('bg-warning');
                break;
            case 'TO SEND':
                this.el.nativeElement.classList.add('bg-warning');
                break;
            case 'IN PROGRESS':
                this.el.nativeElement.classList.add('bg-warning');
                break;
            case 'WAITING_TRUE':
                this.el.nativeElement.classList.add('bg-warning');
                break;
            case 'DONE':
                this.el.nativeElement.classList.add('bg-success');
                break;
            case 'NOT APPLICABLE':
                this.el.nativeElement.classList.add('bg-success');
                break;
            default:
                this.el.nativeElement.classList.add('bg-danger');
                break;
        }
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", contract_1.Workpackage)
    ], ContractStatusDirective.prototype, "contractStatus", void 0);
    ContractStatusDirective = __decorate([
        core_1.Directive({
            selector: '[contractStatus]',
            providers: [contract_service_1.ContractService]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _a || Object, typeof (_b = typeof core_1.Renderer !== "undefined" && core_1.Renderer) === "function" && _b || Object, contract_service_1.ContractService])
    ], ContractStatusDirective);
    return ContractStatusDirective;
    var _a, _b;
}());
exports.ContractStatusDirective = ContractStatusDirective;
//# sourceMappingURL=contract-status.directive.js.map