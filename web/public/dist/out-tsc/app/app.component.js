"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var contract_service_1 = require("./contract/service/contract.service");
var auth_service_1 = require("./auth/auth.service");
var AppComponent = /** @class */ (function () {
    function AppComponent(router, authService, contractService) {
        this.router = router;
        this.authService = authService;
        this.contractService = contractService;
        this.title = 'OVD - Manager';
        authService.handleAuthentication();
    }
    ;
    AppComponent.prototype.search = function (searchTerm) {
        this.router.navigate(["contract/search/" + searchTerm]);
    };
    AppComponent.prototype.login = function () {
        this.authService.login();
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.css'],
            providers: [contract_service_1.ContractService, auth_service_1.AuthService]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof router_1.Router !== "undefined" && router_1.Router) === "function" && _a || Object, auth_service_1.AuthService, contract_service_1.ContractService])
    ], AppComponent);
    return AppComponent;
    var _a;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map