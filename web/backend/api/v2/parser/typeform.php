<?php
class TypeFormParser {
    
    public static function getQuestions($jsonResult){
        return $jsonResult->questions;
    }
    public static function getHttpStats($jsonResult){
        $status = $jsonResult->http_status; 
        if($jsonResult->http_status == null){
            $status = 500;
        }
        return $status;
    }
    public static function getCompletedAnswers($jsonResult){
        $responses = $jsonResult->responses;
        if($responses != null){
            $responsesCompleted = array_filter($responses, function($resp){
                return $resp->completed == 1; 
            });
        }
        return $responsesCompleted;
    }
}