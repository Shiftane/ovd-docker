<?php

class Auth{
    private $logger;
    public function __construct($container)
    {
        $this->logger = $container->get('logger');
    }
    
    function isAuthorized($requestedRole = 'user'){
        return function($request, $response, $next) use ($requestedRole){
            $this->logger->info("Requested access for : ".$requestedRole);
            $request = $request->withAttribute('foo', 'bar');
            $response = $next($request, $response);
            return $response;
        };

    }
}