<?php
// Application middleware
//require_once('auth.php');
// e.g: $app->add(new \Slim\Csrf\Guard);



$app->add(function($request, $response, $next){
        $excludedAuthUrls = array(
                '/api/v2/contracts/webhook/typeform'
        );
        // url is in the excluded authenticate url
        if(in_array($_SERVER['REQUEST_URI'], $excludedAuthUrls)){
                $response = $next($request, $response);
                $authorized = true;
        }

        // this could be a MYSQL query that parses an API Key table, for example
        else if(!$request->getHeader('X-API-KEY')){
                $body = $response->getBody();
                $body->write('{"error":{"text": "api key not sent" }}');
                $response = $response->withHeader('X-Authenticated','False');
                $response = $response->withStatus(302);
                $authorized = false;
        }else{
            $api_key = $request->getHeader('X-API-KEY');
            $this->logger->addInfo('API KEY : '.$api_key[0]);
            if($api_key[0] == '612e648bf9594adb50844cad6895f2cf') {
                $authorized = true;
                $response = $next($request, $response);
            } else {
                    $body = $response->getBody();
                    $body->write('{"error":{"text": "api key invalid" }}');
                    $response = $response->withHeader('X-Authenticated','False');
                    $response = $response->withStatus(302);
                    $authorized = false;

            }    
        }
        

        if(!$authorized){ //key is false
                // dont return 403 if you request the home page
                $req = $_SERVER['REQUEST_URI'];
                if ($req != "/") {
                    $response = $response->withStatus(302);
                }
        }
        return $response;
});
