<?php
// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], Monolog\Logger::DEBUG));
    return $logger;
};

// Statics
require_once('utils.php');
require_once('encoder.php');
require_once('parser/typeform.php');

// DB
require_once('db/mapping.php');
require_once('db/workpackage.php');
require_once('db/user.php');
require_once('db/workpackagestatus.php');
require_once('db/action.php');
require_once('db/formcache.php');
require_once('db/contract.php');
require_once('db/contract-typeform.php');
require_once('auth/Auth.php');
require_once('controllers/ContractController.php');



$container['db'] = function ($c) {
    $db = $c['settings']['db'];
    $pdo = new PDO("mysql:host=" . $db['host'] . ";dbname=" . $db['dbname'],
        $db['user'], $db['pass']);
    $pdo->exec("set names utf8");
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    //$pdo->exec("SET CHARACTER SET utf8");
    return $pdo;
};


