<?php

class ContractController
{
   protected $container;
   protected $logger;
   public $db;

   // constructor receives container instance
   public function __construct(Interop\Container\ContainerInterface $container) {
       $this->container = $container;
       $this->logger = $container['logger'];
       $this->db = $container['db'];
       DataMapper::init($this->db, $this->logger);
   }

   public function getContracts($request, $response, $args) {
        $all = ($request->getQueryParam("all") == "true")?true:false;
        $contracts = ContractTypeformMapper::getTypeformContracts($all);
        $resultsParsed = [];
        foreach ($contracts as $key => $contract) {
            $newContract = new StdClass();
            $newContract->id_hash = $contract["hash"];
            $newContract->data = json_decode($contract["data"]);
            array_push($resultsParsed, self::fullfillContract($newContract));
        }
        //Example to get attribute
        $foo = $request->getAttribute('foo');

        return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($resultsParsed));
   }

    public function archiveContract($request, $response, $args){
        $id_hash = $args['hash'];
        ContractTypeformMapper::archiveContract($id_hash);
        return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write('{"status":"success"}');
    }

    public function getContractData($request, $response, $args){
        $id_hash = $args['hash'];
        $contract = ContractTypeformMapper::getContract($id_hash);
        return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write($contract["data"]);
    }

    public function updateContractData($request, $response, $args){
        $parsedBody = $request->getParsedBody();
        $id_hash = $args['hash'];
        if(ContractTypeformMapper::updateContractData($id_hash, $parsedBody)){
            $return = '{"status":"success"}';
        }else{
            $return = '{"status":"error"}';
        }

        return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write($return);
    }

   private function fullfillContract($contract){
        $contractFormatted = new StdClass();
        $contractFormatted->id = $contract->id_hash;
        $contractFormatted->questionsAnswers = [];

        $answers = $contract->data->form_response->answers;
        $fields = $contract->data->form_response->definition->fields;

        $questionsAnswers = [];
        foreach ($answers as $key => $answer) {
            $questionAnswer = new StdClass();
            $questionAnswer->answer = self::getAnswer($answer);
            $questionAnswer->id = $answer->field->id;
            foreach ($fields as $key => $field) {
                if($field->id == $answer->field->id){
                    $questionAnswer->field = $field->title; 
                }
            }
            array_push($contractFormatted->questionsAnswers, $questionAnswer);
        }

       return $contractFormatted;
   }

   private function getAnswer($answer){
       switch($answer->type){
            case "text" : return $answer->text; 
            case "choices" : return $answer->choices->labels;
            case "email" : return $answer->email;
            case "boolean" : return $answer->boolean;
            case "date" : return $answer->date;
            case "choice" : return (property_exists($answer->choice, 'label')?$answer->choice->label:$answer->choice->other);
            default : return "ERREUR DE TYPE";
       }
   }

  
}    