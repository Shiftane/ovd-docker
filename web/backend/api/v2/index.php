<?php

if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $file = __DIR__ . $_SERVER['REQUEST_URI'];
    if (is_file($file)) {
        return false;
    }
}
if (endsWith($_SERVER['SERVER_NAME'], "locateme.ch")){
    $ENV = 'PROD';
}else{
    $ENV = 'TEST';
}

require __DIR__ . '/../../app/vendor/autoload.php';

session_start();

// Instantiate the app
$settings = require __DIR__ . '/config/settings_'.$ENV.'.php';


$app = new \Slim\App($settings);


// Set up dependencies
require __DIR__ . '/dependencies.php';

// Register middleware
require __DIR__ . '/middleware.php';

// Register routes
require __DIR__ . '/routes.php';

// Run app
$app->run();


function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}
?>