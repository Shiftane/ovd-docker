<?php
class DataMapper {
    public static $db;
    public static $logger;
    
    public static function init($db, $logger)
    {
        self::$db = $db;
        self::$logger = $logger;
    }   
}