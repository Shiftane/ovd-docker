<?php
require_once('data.php');

class FormCacheMapper extends DataMapper{
    
    static function getLastFormCache($formId){
        $query = self::$db->prepare("SELECT * FROM form_cache where form_id = :formId order by date desc limit 1 ");
        $query->bindParam(':formId', $formId);
        $query->execute();
        $results=$query->fetchAll(PDO::FETCH_ASSOC);
        //self::$logger->addInfo('results : '.json_encode($results[0]));
        return json_encode($results[0]);
    }
    static function insertFormCache($formId, $result){
        $query = self::$db->prepare("INSERT INTO form_cache (form_id, data) VALUES (:formId, :data)");
        $query->bindParam(':formId',$formId);
        $query->bindParam(':data',$result);
        $query->execute();
        return self::$db->lastInsertId();
    }
}