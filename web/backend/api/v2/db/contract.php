<?php
require_once('data.php');

class ContractMapper extends DataMapper{
    
    static function getContracts(){
        $query = self::$db->prepare("select * from contract");
        $query->execute();
        $results=$query->fetchAll(PDO::FETCH_ASSOC);
        return $results;
    }
    static function saveContract($data){
        $query = self::$db->prepare("insert into contract set ");
        $query->execute();
        $results=$query->fetchAll(PDO::FETCH_ASSOC);
        return $results;
    }
}