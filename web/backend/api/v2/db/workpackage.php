<?php
require_once('data.php');

class WorkPackageMapper extends DataMapper{
    
    static function getWorkPackages(){
        $query = self::$db->prepare("SELECT * FROM work_package wp order by wp.sorting_priority");
        $query->execute();
        $results=$query->fetchAll(PDO::FETCH_ASSOC);
        return $results;
    }
    static function saveOrUpdateWorkPackage($workPackageToSave){
        $query = self::$db->prepare("insert into work_package (id, name, needed_fields, needed_condition, sorting_priority) VALUES (:id, :name, :needed_fields, :needed_condition, :sorting_priority) ON DUPLICATE KEY UPDATE name=:name, needed_fields=:needed_fields, needed_condition=:needed_condition, sorting_priority=:sorting_priority");
        $query->bindParam(':id',$workPackageToSave->id);
        $query->bindParam(':name',$workPackageToSave->name);
        $query->bindParam(':needed_fields',$workPackageToSave->needed_fields);
        $query->bindParam(':needed_condition',$workPackageToSave->needed_condition);
        $query->bindParam(':sorting_priority',$workPackageToSave->sorting_priority);
        $query->execute();
        return self::$db->lastInsertId();
    }
    static function deleteWorkPackage($workPackageToDelete){
        $query = self::$db->prepare("delete from work_package where id=:id");
        $query->bindParam(':id',$workPackageToDelete->id);
        $count = $query->execute();
        return $count;
    }
}