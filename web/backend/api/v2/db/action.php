<?php
require_once('data.php');

class ActionMapper extends DataMapper{
    
    static function getActions($wpid){
        $query = self::$db->prepare("select * from action where work_package_fk=:wpid");
        $query->bindParam(':wpid',$wpid);
        $query->execute();
        $results=$query->fetchAll(PDO::FETCH_ASSOC);
        //self::$logger->addInfo("DB RESULTS : ".$results);
        return json_encode($results);
    }

    static function getActionWithWorkpackage(){
        $query = self::$db->prepare("SELECT wp.id as wp_id, wp.name, a.* FROM action a, work_package wp WHERE a.work_package_fk = wp.id");
        $query->execute();
        $results=$query->fetchAll(PDO::FETCH_ASSOC);
        return $results;

    }

    static function saveOrUpdateAction($actionToSave){
        $query = self::$db->prepare("insert into action (work_package_fk, action, action_name, condition_attribut, condition_value, data, comments, type) VALUES (:work_package_fk, :action, :action_name, :condition_attribut, :condition_value, :data, :comments, :type) ON DUPLICATE KEY UPDATE work_package_fk=:work_package_fk, type=:type, condition_attribut=:condition_attribut, condition_value=:condition_value, data=:data, comments=:comments, action=:action, action_name=:action_name");
        $query->bindParam(':work_package_fk',$actionToSave->work_package_fk);
        $query->bindParam(':type',$actionToSave->type);
        $query->bindParam(':action',$actionToSave->action);
        $query->bindParam(':action_name',$actionToSave->action_name);
        $query->bindParam(':condition_attribut',$actionToSave->condition_attribut);
        $query->bindParam(':condition_value',$actionToSave->condition_value);
        $query->bindParam(':data',$actionToSave->data);
        $query->bindParam(':comments',$actionToSave->comments);
        $query->execute();
        return self::$db->lastInsertId();
    }

    static function deleteAction($id){
        $query = self::$db->prepare("delete from action where id=:id");
        $query->bindParam(':id',$id);
        $count = $query->execute();
        return $count;
    }
}