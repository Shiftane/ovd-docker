<?php
require_once('data.php');

class WorkPackageStatusMapper extends DataMapper{
    
    static function getWorkPackageStatuses(){
        $query = self::$db->prepare("SELECT  * FROM work_package_status");
        $query->execute();
        $results=$query->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($results);
    }
    
    static function insertWorkPackageStatus($wpstatus){
        $query = self::$db->prepare("INSERT INTO work_package_status (form_id, work_package_id,status, comments) VALUES (:formId, :workPackageId, :status, :comment)");
        $query->bindParam(':formId',$wpstatus->formId);
        $query->bindParam(':workPackageId',$wpstatus->workPackageId);
        $query->bindParam(':status',$wpstatus->status);
        $query->bindParam(':comment',$wpstatus->comment);
        $query->execute();
        return self::$db->lastInsertId();
    }
}