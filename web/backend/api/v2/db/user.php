<?php
require_once('data.php');

class UserMapper extends DataMapper{
    
    static function getUser($email, $pass){
        $query = self::$db->prepare("SELECT * FROM users where user_email='$email'");
        $query->execute();
        $result=$query->fetch();
        return $result;
    }
    static function insertUser($customer){
        $query = self::$db->prepare("INSERT INTO users (user_email, user_name, user_password, user_prenom) VALUES (:email, :name, :password, :prenom)");
        $query->bindParam(':email',$customer->email);
        $query->bindParam(':name',$customer->name);
        $query->bindParam(':password',$customer->password);
        $query->bindParam(':prenom',$customer->prenom);
        $query->execute();
        return self::$db->lastInsertId();
    }
}