<?php
require_once('data.php');

class ContractTypeformMapper extends DataMapper{
    
   
    static function getTypeformContracts($showArchived = false){
        $where = ($showArchived)?"":"WHERE archive=0";
        $query = self::$db->prepare("select * from contract_typeform ".$where);
        //$query = self::$db->select()->from("contract_typeform");
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);
        return $results;
    }

    static function getContract($id_hash){
        $query = self::$db->prepare("select * from contract_typeform where hash = :id_hash ");
        $query->bindParam(":id_hash",$id_hash);
        $query->execute();
        $result = $query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    
    static function saveTypeformContract($data){
        //self::$logger->addInfo("Data : ".$data);
        $hash = md5($data.(new Date().getTimestamp()));
        $query = self::$db->prepare("insert into contract_typeform (data, hash, insertion_date) VALUES (:data, :hash, NOW())");
        $query->bindParam(':data', $data);
        $query->bindParam(':hash', $hash);
        $query->execute();
        return self::$db->lastInsertId();
    }
    static function archiveContract($id_hash){
        //$query = self::$db->update("contract")->set("archive",1)->where("hash","=",$id_hash);
        $query = self::$db->prepare("update contract_typeform set archive=1 where hash = :id_hash");
        $query->bindParam(":id_hash",$id_hash);
        $query->execute();
        return true;
    }

    static function updateContractData($id_hash, $data){
        $jsonData = json_encode($data);
        $query = self::$db->prepare("update contract_typeform set data=:data where hash = :id_hash");
        $query->bindParam(":id_hash",$id_hash);
        $query->bindParam(":data", $jsonData);
        $query->execute();
        return true;
    }
}