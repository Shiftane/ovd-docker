<?php
require_once('data.php');

class MappingMapper extends DataMapper{
    
    static function getMapping($formProvider){
        $query = self::$db->prepare("select * from form_mapping where provider = :provider");
        $query->bindParam(':provider',$formProvider);
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($results);
    }
    static function getMappings(){
        $query = self::$db->prepare("select * from form_mapping");
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);
        return $results;
    }
    static function getMappingsByInternalName($internal_name){
        $query = self::$db->prepare("select * from form_mapping where internal_name = :internal_name");
        $query->bindParam(':internal_name',$internal_name);
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);
        return $results;
    }
    static function searchMappingsByInternalName($internal_name){
        $internal_name = "%".$internal_name."%";
        $query = self::$db->prepare("select * from form_mapping where internal_name like :internal_name");
        $query->bindParam(':internal_name',$internal_name);
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);
        return $results;
    }
    static function saveOrUpdateMapping($mappingToSave){
        $query = self::$db->prepare("insert into form_mapping (provider, type, provider_key, internal_name, value_index) VALUES (:provider, :type, :provider_key, :internal_name, :value_index) ON DUPLICATE KEY UPDATE provider=:provider, type=:type, provider_key=:provider_key, internal_name=:internal_name, value_index=:value_index");
        $query->bindParam(':provider',$mappingToSave->provider);
        $query->bindParam(':type',$mappingToSave->type);
        $query->bindParam(':provider_key',$mappingToSave->provider_key);
        $query->bindParam(':internal_name',$mappingToSave->internal_name);
        $query->bindParam(':value_index',$mappingToSave->value_index);
        $query->execute();
        return self::$db->lastInsertId();
    }
    static function deleteMapping($mappingToDelete){
        $query = self::$db->prepare("delete from form_mapping where provider=:provider and internal_name=:internal_name and value_index=:value_index");
        $query->bindParam(':provider',$mappingToDelete->provider);
        $query->bindParam(':internal_name',$mappingToDelete->internal_name);
        $query->bindParam(':value_index',$mappingToDelete->value_index);
        $count = $query->execute();
        return $count;
    }
    
    
}