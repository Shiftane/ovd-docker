<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../../../logs/app.log',
        ],
        
        // DB settings
        'db' => [
            'host' => 'localhost',
            'user' => 'onvousdemenage',
            'pass' => 'Dozr?195',
            'dbname' => 'onvousdemenage',
        ],
    ],
];
